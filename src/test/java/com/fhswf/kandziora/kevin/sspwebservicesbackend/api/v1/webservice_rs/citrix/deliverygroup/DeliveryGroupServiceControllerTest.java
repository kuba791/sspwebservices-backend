package com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.webservice_rs.citrix.deliverygroup;

import com.fhswf.kandziora.kevin.sspwebservicesbackend.utilities.JsonUtilities;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@ExtendWith(MockitoExtension.class)
class DeliveryGroupServiceControllerTest
{
    @InjectMocks
    @Spy
    private DeliveryGroupServiceController cut;

    private static final String DELIVERY_GROUP_JSON = "{\n" + "    \"deliveryGroup\": {\n" + "        \"name\": \"Lawyers only\",\n" + "        \"description\": \"Just 4 Lawyers\",\n" + "        \"turnOnMachinesAfterCreation\": false,\n" + "        \"assignedMachineCatalog\": {\n" + "            \"id\": 3,\n" + "            \"user\": {\n" + "                \"id\": 1,\n" + "                \"password\": \"$2a$10$vf6cJ9SfsEUaeAKuX4c0V.HsEq03LjqNxfqMYZ5qCACBhS84xHaQq\",\n" + "                \"mail\": \"kekan@fh.de\",\n" + "                \"active\": true,\n" + "                \"userRole\": \"ADMIN\"\n" + "            },\n" + "            \"uid\": 12,\n" + "            \"name\": \"Lawyers\",\n" + "            \"description\": null,\n" + "            \"organizationUnit\": null,\n" + "            \"numbersOfVms\": null,\n" + "            \"numbersOfProcessors\": null,\n" + "            \"memoryInMb\": null,\n" + "            \"accountNamingSchema\": null,\n" + "            \"accountNamingSchemaType\": null\n" + "        },\n" + "        \"assignedMachines\": [\n" + "            {\n" + "                \"name\": \"lawyer-001\",\n" + "                \"assignedUsers\": [\n" + "                    {\n" + "                        \"name\": \"John Doe\",\n" + "                        \"userPrincipalName\": \"jdoe@win2016s01.fhswf.de\",\n" + "                        \"objectGuid\": \"43b5da30-2370-414d-9213-888d47e89231\",\n" + "                        \"sid\": \"S-1-5-21-2649842711-4096779769-2815699909-1657\"\n" + "                    }\n" + "                ],\n" + "                \"powerState\": 4,\n" + "                \"powerActions\": [\n" + "                    \"Reset\",\n" + "                    \"Restart\",\n" + "                    \"Resume\",\n" + "                    \"Shutdown\",\n" + "                    \"Suspend\",\n" + "                    \"TurnOff\",\n" + "                    \"TurnOn\"\n" + "                ]\n" + "            },\n" + "            {\n" + "                \"name\": \"lawyer-002\",\n" + "                \"assignedUsers\": [\n" + "                    {\n" + "                        \"name\": \"Johanna Schmitt\",\n" + "                        \"userPrincipalName\": \"jschmitt@win2016s01.fhswf.de\",\n" + "                        \"objectGuid\": \"181f639e-c1cd-429b-8435-f47b98616c20\",\n" + "                        \"sid\": \"S-1-5-21-2649842711-4096779769-2815699909-1656\"\n" + "                    },\n" + "                    {\n" + "                        \"name\": \"Tom Hiddleston\",\n" + "                        \"userPrincipalName\": \"thiddle@win2016s01.fhswf.de\",\n" + "                        \"objectGuid\": \"31b128a2-fab1-4f5a-92ee-196229585687\",\n" + "                        \"sid\": \"S-1-5-21-2649842711-4096779769-2815699909-1658\"\n" + "                    }\n" + "                ],\n" + "                \"powerState\": 4,\n" + "                \"powerActions\": [\n" + "                    \"Reset\",\n" + "                    \"Restart\",\n" + "                    \"Resume\",\n" + "                    \"Shutdown\",\n" + "                    \"Suspend\",\n" + "                    \"TurnOff\",\n" + "                    \"TurnOn\"\n" + "                ]\n" + "            }\n" + "        ]\n" + "    }\n" + "}";

    @Test
    void testExtractDeliveryGroup()
    {
        // process
        DeliveryGroupDTO result = JsonUtilities.convertRawValueToDeliveryGroupJson(DELIVERY_GROUP_JSON);

        // assert & verify

        // Delivery group
        assertThat(result.getName(), is("Lawyers only"));
        assertThat(result.getDescription(), is("Just 4 Lawyers"));
        assertThat(result.getAssignedMachines().size(), is(2));
        assertThat(result.isTurnOnMachinesAfterCreation(), is(false));

        // Assigned machines - assigned machine 1
        assertThat(result.getAssignedMachines().get(0).getName(), is("lawyer-001"));
        assertThat(result.getAssignedMachines().get(0).getPowerState(), is(4));
        assertThat(result.getAssignedMachines().get(0).getAssignedUsers().size(), is(1));
        assertThat(result.getAssignedMachines().get(0).getPowerActions().size(), is(7));
        // Assigned users - user 1
        assertThat(result.getAssignedMachines().get(0).getAssignedUsers().get(0).getName(), is("John Doe"));
        assertThat(result.getAssignedMachines().get(0).getAssignedUsers().get(0).getUserPrincipalName(), is("jdoe@win2016s01.fhswf.de"));
        assertThat(result.getAssignedMachines().get(0).getAssignedUsers().get(0).getObjectGuid(), is("43b5da30-2370-414d-9213-888d47e89231"));
        assertThat(result.getAssignedMachines().get(0).getAssignedUsers().get(0).getSid(), is("S-1-5-21-2649842711-4096779769-2815699909-1657"));

        // Assigned machines - assigned machine 1
        assertThat(result.getAssignedMachines().get(1).getName(), is("lawyer-002"));
        assertThat(result.getAssignedMachines().get(1).getPowerState(), is(4));
        assertThat(result.getAssignedMachines().get(1).getAssignedUsers().size(), is(2));
        assertThat(result.getAssignedMachines().get(1).getPowerActions().size(), is(7));
        // Assigned users - user 1
        assertThat(result.getAssignedMachines().get(1).getAssignedUsers().get(0).getName(), is("Johanna Schmitt"));
        assertThat(result.getAssignedMachines().get(1).getAssignedUsers().get(0).getUserPrincipalName(), is("jschmitt@win2016s01.fhswf.de"));
        assertThat(result.getAssignedMachines().get(1).getAssignedUsers().get(0).getObjectGuid(), is("181f639e-c1cd-429b-8435-f47b98616c20"));
        assertThat(result.getAssignedMachines().get(1).getAssignedUsers().get(0).getSid(), is("S-1-5-21-2649842711-4096779769-2815699909-1656"));
        // Assigned users - user 2
        assertThat(result.getAssignedMachines().get(1).getAssignedUsers().get(1).getName(), is("Tom Hiddleston"));
        assertThat(result.getAssignedMachines().get(1).getAssignedUsers().get(1).getUserPrincipalName(), is("thiddle@win2016s01.fhswf.de"));
        assertThat(result.getAssignedMachines().get(1).getAssignedUsers().get(1).getObjectGuid(), is("31b128a2-fab1-4f5a-92ee-196229585687"));
        assertThat(result.getAssignedMachines().get(1).getAssignedUsers().get(1).getSid(), is("S-1-5-21-2649842711-4096779769-2815699909-1658"));
    }
}