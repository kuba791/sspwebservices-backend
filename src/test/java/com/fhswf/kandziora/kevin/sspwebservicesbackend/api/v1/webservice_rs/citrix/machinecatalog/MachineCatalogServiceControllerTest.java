package com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.webservice_rs.citrix.machinecatalog;

import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.machinecatalog.NamingSchemaType;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.utilities.JsonUtilities;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@ExtendWith(MockitoExtension.class)
class MachineCatalogServiceControllerTest
{
    public static final String MACHINE_CATALOG_JSON = "{\n" + "    \"catalog\": {\n" + "        \"accountNamingSchema\": \"lawyer-###\",\n" + "        \"accountNamingSchemaType\": \"NUMERIC\",\n" + "        \"description\": \"Lawyers-catalog\",\n" + "        \"name\": \"Lawyers\",\n" + "        \"memoryInMb\": 4096,\n" + "        \"numbersOfProcessors\": 2,\n" + "        \"numbersOfVms\": 2\n" + "    }\n" + "}";

    @InjectMocks
    @Spy
    private MachineCatalogServiceController cut;

    @Test
    void testExtractMachineCatalogJson()
    {
        // process
        MachineCatalogDTO result = JsonUtilities.extractMachineCatalogJson(MACHINE_CATALOG_JSON);

        // assert
        assertThat(result.getName(), is("Lawyers"));
        assertThat(result.getAccountNamingSchema(), is("lawyer-###"));
        assertThat(result.getAccountNamingSchemaType(), is(NamingSchemaType.NUMERIC));
        assertThat(result.getDescription(), is("Lawyers-catalog"));
        assertThat(result.getMemoryInMb(), is(4096));
        assertThat(result.getNumbersOfProcessors(), is(2));
        assertThat(result.getNumbersOfVms(), is(2));
    }
}