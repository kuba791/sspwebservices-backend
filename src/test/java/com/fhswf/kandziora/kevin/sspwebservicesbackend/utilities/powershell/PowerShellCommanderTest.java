package com.fhswf.kandziora.kevin.sspwebservicesbackend.utilities.powershell;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.user_rs.ActiveDirectoryUserDTO;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.webservice_rs.citrix.deliverygroup.DeliveryGroupDTO;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.webservice_rs.citrix.machine.MachineDTO;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.webservice_rs.citrix.machinecatalog.MachineCatalogDTO;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user.ActiveDirectoryUser;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.deliverygroup.DeliveryGroup;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.machine.Machine;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.machinecatalog.MachineCatalog;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.machinecatalog.NamingSchemaType;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.utilities.JsonUtilities;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

@ExtendWith(MockitoExtension.class)
class PowerShellCommanderTest
{
    private static final String AD_USERS_JSON = "[\n" + " {\n" + "\t \"Name\":  \"Johanna Schmitt\",\n" + "\t \"UserPrincipalName\":  \"jschmitt@win2016s01.fhswf.de\",\n" + "\t \"ObjectGUID\":  \"181f639e-c1cd-429b-8435-f47b98616c20\",\n" + "\t \"SID\":  {\n" + "\t\t\t\t \"BinaryLength\":  28,\n" + "\t\t\t\t \"AccountDomainSid\":  \"S-1-5-21-2649842711-4096779769-2815699909\",\n" + "\t\t\t\t \"Value\":  \"S-1-5-21-2649842711-4096779769-2815699909-1656\"\n" + "\t\t\t }\n" + " },\n" + " {\n" + "\t \"Name\":  \"John Doe\",\n" + "\t \"UserPrincipalName\":  \"jdoe@win2016s01.fhswf.de\",\n" + "\t \"ObjectGUID\":  \"43b5da30-2370-414d-9213-888d47e89231\",\n" + "\t \"SID\":  {\n" + "\t\t\t\t \"BinaryLength\":  28,\n" + "\t\t\t\t \"AccountDomainSid\":  \"S-1-5-21-2649842711-4096779769-2815699909\",\n" + "\t\t\t\t \"Value\":  \"S-1-5-21-2649842711-4096779769-2815699909-1657\"\n" + "\t\t\t }\n" + " },\n" + " {\n" + "\t \"Name\":  \"Tom Hiddleston\",\n" + "\t \"UserPrincipalName\":  \"thiddle@win2016s01.fhswf.de\",\n" + "\t \"ObjectGUID\":  \"31b128a2-fab1-4f5a-92ee-196229585687\",\n" + "\t \"SID\":  {\n" + "\t\t\t\t \"BinaryLength\":  28,\n" + "\t\t\t\t \"AccountDomainSid\":  \"S-1-5-21-2649842711-4096779769-2815699909\",\n" + "\t\t\t\t \"Value\":  \"S-1-5-21-2649842711-4096779769-2815699909-1658\"\n" + "\t\t\t }\n" + " }\n" + "]";
    private static final String MACHINES_OF_SPECIFIC_CATALOG = "[\n" + "    {\n" + "       \"Uid\": 10,\n" + "       \"HostedMachineName\": \"lawyer-001\",\n" + "        \"AssociatedUserUPNs\": [\n" + "            \"jdoe@win2016s01.fhswf.de\",\n" + "            \"jschmitt@win2016s01.fhswf.de\"\n" + "        ],\n" + "        \"PowerState\": 4,\n" + "        \"SupportedPowerActions\": [\n" + "            \"Reset\",\n" + "            \"Restart\",\n" + "            \"Resume\",\n" + "           \"Shutdown\",\n" + "            \"Suspend\",\n" + "           \"TurnOff\",\n" + "            \"TurnOn\"\n" + "       ]\n" + "    }\n" + "]\n";

    @Test
    void testCreateMachineCatalogParamExpression()
    {
        // prepare
        MachineCatalog catalog = new MachineCatalog();
        catalog.setOrganizationUnit("CN=Users,DC=win2016s01,DC=fhswf,DC=de");
        catalog.setAccountNamingSchema("test-###");
        catalog.setAccountNamingSchemaType(NamingSchemaType.NUMERIC);
        catalog.setDescription("Description");
        catalog.setName("Test");
        catalog.setMemoryInMb(4096);
        catalog.setNumbersOfProcessors(1);
        catalog.setNumbersOfVms(1);

        // process
        String result = PowerShellCommander.createMachineCatalogParamExpression(catalog);
        System.out.println("Result: " + result);

        // assert & verify
    }

    @Test
    void testBuildPowerShellCommand()
    {
        // prepare
        String scriptPath = PowerShellCommander.PS_CREATE_NEW_MACHINE_CATALOG_WITH_NEW_AD_COMPUTER_ACCOUNTS_FILE;
        String params = "-catalogName \"Test\" -description \"Description\" -namingScheme \"test-###\" -namingSchemeType \"NUMERIC\" -orgUnit \"CN=Users,DC=win2016s01,DC=fhswf,DC=de\" -vCpuCount 1 -vRAM 4096 -countOfAccounts 1";

        String expected = "powershell.exe" + " " + scriptPath + " " + params;

        // process
        String result = PowerShellCommander.buildPowerShellCommand(scriptPath, params);

        // assert & verify
        assertThat(expected, is(result));
    }

    @Test
    void testIsNumericWithPositiveValue()
    {
        // prepare
        String positiveNumber = "42";

        // process
        boolean result = PowerShellCommander.isNumeric(positiveNumber);

        // assert & verify
        assertThat(result, is(true));
    }

    @Test
    void testIsNumericWithNegativeValue()
    {
        // prepare
        String negativeNumber = "-1";

        // process
        boolean result = PowerShellCommander.isNumeric(negativeNumber);

        // assert & verify
        assertThat(result, is(true));
    }

    @Test
    void testConvertActiveDirectoryUsersResultToJson()
    {
        // process
        List<ActiveDirectoryUser> result = JsonUtilities.convertPowerShellActiveDirectoryUsersResult(AD_USERS_JSON);

        // assert
        assertThat(result, hasSize(3));
    }

    @Test
    void testConvertMachinesResultToJson()
    {
        // process
        List<Machine> result = JsonUtilities.convertPowerShellMachinesResultToJson(MACHINES_OF_SPECIFIC_CATALOG);

        // prints
        Machine machine = result.get(0);

        System.out.println(machine.getName());
        System.out.println(machine.getDeliveryGroupName());
        System.out.println(machine.getCatalogName());
        System.out.println(machine.getAssignedUsers());
        System.out.println(machine.getPowerActions());
        System.out.println(machine.getPowerState());

        // assert
        assertThat(result, hasSize(1));
        assertThat(result.get(0).getName(), is("lawyer-001"));
    }

    @Test
    void testConvertToJsonObject() throws JsonProcessingException
    {
        // prepare
        ActiveDirectoryUserDTO userJson1 = new ActiveDirectoryUserDTO("John Dow",
                "jdow@win2016s01.fhswf.de", "some-object-guid-of-jdoe", "some-sid-of-jdoe");
        ActiveDirectoryUserDTO userJson2 = new ActiveDirectoryUserDTO("Johanna Schmitt",
                "jschmitt@win2016s01.fhswf.de", "some-object-guid-of-jschmitt", "some-sid-of-jschmitt");

        MachineDTO machineDTO1 = new MachineDTO(1L, "lawyer-001", "Lawyer", 56L, Arrays.asList(userJson1, userJson2),
                4, Arrays.asList("TurnOff", "TurnOn"));
        MachineDTO machineDTO2 = new MachineDTO(2L, "lawyer-002", "Lawyer", 56L, Arrays.asList(userJson2, userJson1),
                3, Arrays.asList("TurnOff", "TurnOn"));
        MachineDTO machineDTO3 = new MachineDTO(3L, "lawyer-003", "Lawyer", 56L, Arrays.asList(userJson1),
                5, Arrays.asList("TurnOff", "TurnOn"));

        MachineCatalogDTO catalogJson = new MachineCatalogDTO(12L, "Lawyers", "Lawyers catalog",
                "some org unit", 1, 2, 4096,
                "lawyer-###", NamingSchemaType.NUMERIC);

        DeliveryGroupDTO groupJson = new DeliveryGroupDTO("Lawyers only", "Just 4 lawyers",
                true, catalogJson, Arrays.asList(machineDTO1, machineDTO2, machineDTO3));

        // process
        String result = new ObjectMapper().writeValueAsString(groupJson);

        System.out.println(result);
    }

    @Test
    void testCreateUserAssignedMachinesMapParamOneMachineOneUser()
    {
        // prepare
        DeliveryGroup dg = new DeliveryGroup();

        ActiveDirectoryUserDTO u1 = new ActiveDirectoryUserDTO();
        u1.setSid("u1-sid");

        MachineDTO m1 = new MachineDTO();
        m1.setName("machine-1");
        m1.setAssignedUsers(Arrays.asList(u1));

        dg.setMachines(Arrays.asList(m1));

        // process
        String result = PowerShellCommander.createUserAssignedMachinesMapParam(dg);

        // assert & verify
        assertThat(result, is("@{'machine-1'=@('u1-sid')}"));
    }

    @Test
    void testCreateUserAssignedMachinesMapParamOneMachineMultipleUser()
    {
        // prepare
        DeliveryGroup dg = new DeliveryGroup();

        ActiveDirectoryUserDTO u1 = new ActiveDirectoryUserDTO();
        u1.setSid("u1-sid");

        ActiveDirectoryUserDTO u2 = new ActiveDirectoryUserDTO();
        u2.setSid("u2-sid");

        ActiveDirectoryUserDTO u3 = new ActiveDirectoryUserDTO();
        u3.setSid("u3-sid");

        MachineDTO m1 = new MachineDTO();
        m1.setName("machine-1");
        m1.setAssignedUsers(Arrays.asList(u1, u2, u3));

        dg.setMachines(Arrays.asList(m1));

        // process
        String result = PowerShellCommander.createUserAssignedMachinesMapParam(dg);

        // assert & verify
        assertThat(result, is("@{'machine-1'=@('u1-sid', 'u2-sid', 'u3-sid')}"));
    }

    @Test
    void testCreateUserAssignedMachinesMapParamMultipleMachinesOneUser()
    {
        // prepare
        DeliveryGroup dg = new DeliveryGroup();

        ActiveDirectoryUserDTO u1 = new ActiveDirectoryUserDTO();
        u1.setSid("u1-sid");

        MachineDTO m1 = new MachineDTO();
        m1.setName("machine-1");
        m1.setAssignedUsers(Arrays.asList(u1));

        MachineDTO m2 = new MachineDTO();
        m2.setName("machine-2");
        m2.setAssignedUsers(Arrays.asList(u1));

        dg.setMachines(Arrays.asList(m1, m2));

        // process
        String result = PowerShellCommander.createUserAssignedMachinesMapParam(dg);

        // assert & verify
        assertThat(result, is("@{'machine-1'=@('u1-sid'); 'machine-2'=@('u1-sid')}"));
    }
    @Test
    void testCreateUserAssignedMachinesMapParamMultipleMachinesMultipleUser()
    {
        // prepare
        DeliveryGroup dg = new DeliveryGroup();

        ActiveDirectoryUserDTO u1 = new ActiveDirectoryUserDTO();
        u1.setSid("u1-sid");

        ActiveDirectoryUserDTO u2 = new ActiveDirectoryUserDTO();
        u2.setSid("u2-sid");

        MachineDTO m1 = new MachineDTO();
        m1.setName("machine-1");
        m1.setAssignedUsers(Arrays.asList(u1, u2));

        MachineDTO m2 = new MachineDTO();
        m2.setName("machine-2");
        m2.setAssignedUsers(Arrays.asList(u1, u2));

        dg.setMachines(Arrays.asList(m1, m2));

        // process
        String result = PowerShellCommander.createUserAssignedMachinesMapParam(dg);

        // assert & verify
        assertThat(result, is("@{'machine-1'=@('u1-sid', 'u2-sid'); 'machine-2'=@('u1-sid', 'u2-sid')}"));
    }

    @Test
    void testCreateDeliveryGroupParamExpression()
    {
        // Prepare
        ActiveDirectoryUserDTO u1 = new ActiveDirectoryUserDTO();
        u1.setSid("S-1-5-21-2649842711-4096779769-2815699909-1656");

        ActiveDirectoryUserDTO u2 = new ActiveDirectoryUserDTO();
        u2.setSid("S-1-5-21-2649842711-4096779769-2815699909-1657");

        ActiveDirectoryUserDTO u3 = new ActiveDirectoryUserDTO();
        u3.setSid("S-1-5-21-2649842711-4096779769-2815699909-1658");

        MachineDTO m1 = new MachineDTO();
        m1.setName("lawyer-001");
        m1.setAssignedUsers(Arrays.asList(u1, u2));

        MachineDTO m2 = new MachineDTO();
        m2.setName("lawyer-002");
        m2.setAssignedUsers(Arrays.asList(u3));

        DeliveryGroup group = new DeliveryGroup();
        group.setTurnOnAddedMachines(false);
        group.setMachines(Arrays.asList(m1, m2));
        group.setCatalogUid(56L);
        group.setDescription("Just 4 Lawyers");
        group.setName("Lawyers only");

        String expected = "-deliveryGroupName 'Lawyers only' -description 'Just 4 Lawyers' -catalogUID 56 -userAssignedMachines @{'lawyer-001'=@('S-1-5-21-2649842711-4096779769-2815699909-1656', 'S-1-5-21-2649842711-4096779769-2815699909-1657'); 'lawyer-002'=@('S-1-5-21-2649842711-4096779769-2815699909-1658')} -turnOnAddedMachines 0";

        // process
        String result = PowerShellCommander.createDeliveryGroupParamExpression(group);

        // assert
        assertThat(result, is(expected));
    }

    @Test
    void testGenerateParamForRetrievingAllMachinesOfUsers()
    {
        // prepare
        List<Integer> catalogUids = Arrays.asList(1, 2, 3);
        String expected = "-catalogUIDs @(1, 2, 3)";

        // process
        String result = PowerShellCommander.generateParamForRetrievingAllMachinesOfUsers(catalogUids);

        // assert
        assertThat(result, is(expected));
    }

    @Test
    void testBuildArrayParamFromIntegerList()
    {
        // prepare
        List<Integer> machineUids = Arrays.asList(1, 2, 3);
        String expected = "@(1, 2, 3)";

        // process
        String result = PowerShellCommander.buildArrayParamFromIntegerList(machineUids);

        // assert
        assertThat(result, is(expected));
    }
}