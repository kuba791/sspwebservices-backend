package com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * Parent class for all user types.
 */
@Entity
@Table(name = "user")
public class User implements Serializable
{
    private static final long serialVersionUID = -6500118501341534296L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "password")
    private String password;

    @Column(name = "mail")
    private String mail;

    @Column(name = "is_active")
    private boolean active;

    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private UserRole userRole;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @MapKeyColumn(name = "property_key")
    @JsonIgnore
    private Map<String, UserProperty> userProperties;

    /**
     * Default constructor
     */
    public User()
    {
        // intentionally empty
    }

    /**
     * Public constructor for the user
     *
     * @param mail     {@link String}
     * @param password {@link String}
     * @param active   {@link boolean}
     */
    public User(String mail, String password, boolean active)
    {
        this.mail = mail;
        this.password = password;
        this.active = active;
        this.userRole = UserRole.USER;
    }

    @Transient
    public void addProperty(String key, String value)
    {
        if (this.userProperties == null)
        {
            this.userProperties = new HashMap<>();
        }

        UserProperty property = this.userProperties.get(key);

        if (property != null)
        {
            property.setValue(value);
        }
        else
        {
            property = new UserProperty();
            property.setUser(this);
            property.setPropertyKey(key);
            property.setValue(value);

            this.userProperties.put(key, property);
        }
    }

    @Transient
    public String getPropertyValue(String propertyKey)
    {
        if (this.userProperties == null)
        {
            return null;
        }

        UserProperty userProperty = this.userProperties.get(propertyKey);
        if (userProperty == null)
        {
            return null;
        }
        return userProperty.getValue();
    }

    @Transient
    public void deleteProperty(String key)
    {
        if (this.userProperties != null)
        {
            this.userProperties.remove(key);
        }
    }

    // ----- GETTERS & SETTERS -----
    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getMail()
    {
        return mail;
    }

    public void setMail(String mail)
    {
        this.mail = mail;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public UserRole getUserRole()
    {
        return userRole;
    }

    public void setUserRole(UserRole userRole)
    {
        this.userRole = userRole;
    }

    public Map<String, UserProperty> getUserProperties()
    {
        return userProperties;
    }

    public void setUserProperties(Map<String, UserProperty> userProperties)
    {
        this.userProperties = userProperties;
    }

    // Override hashCode- and equals-method

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return active == user.active && Objects.equals(id, user.id) && Objects.equals(password, user.password) && Objects.equals(mail, user.mail) && userRole == user.userRole && Objects.equals(userProperties, user.userProperties);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, password, mail, active, userRole, userProperties);
    }
}