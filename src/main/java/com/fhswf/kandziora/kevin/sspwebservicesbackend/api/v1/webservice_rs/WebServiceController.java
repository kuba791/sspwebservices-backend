package com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.webservice_rs;

import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.XenDesktopConfigurationType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

@RestController
@CrossOrigin()
@RequestMapping("/users/web-services")
public class WebServiceController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(WebServiceController.class);

    // ----- Resources -----
    /**
     * Get-resource making all available XenDesktop components public to the user.
     * @return Returns a response containing all available XenDesktop components (Machine-Catalogs, Delivery groups, etc.).
     */
    @GetMapping("/all")
    public ResponseEntity<?> getAllAvailableXenDesktopComponents()
    {
        return ResponseEntity.ok(Arrays.asList(XenDesktopConfigurationType.values()));
    }
}

