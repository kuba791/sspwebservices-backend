package com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatusController
{
    @GetMapping("/status")
    public String getStatus()
    {
        return "FH web service backend is online!";
    }
}
