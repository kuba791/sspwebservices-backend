package com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.webservice_rs.citrix.machine;

import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.machine.Machine;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.service.citrix.DeliverGroupService;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.service.citrix.MachineCatalogService;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.utilities.JsonUtilities;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.utilities.powershell.PowerShellCommander;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.utilities.powershell.PowerShellResponse;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@CrossOrigin()
@RequestMapping("/users/{userId}/web-services/machines")
public class MachineServiceController
{
    private static final Logger LOGGER = Logger.getLogger(MachineServiceController.class);
    protected DeliverGroupService deliverGroupService;
    protected MachineCatalogService machineCatalogService;

    public MachineServiceController(DeliverGroupService deliverGroupService)
    {
        this.deliverGroupService = deliverGroupService;
    }

    /**
     * Get resource to provide all machines of the given user by all delivery group uids found by the user.
     * @param userId ID of the user.
     * @return Returns a response containing all machines created by the user.
     */
    @GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllMachinesOfUser(@PathVariable Long userId)
    {
        List<Integer> machineCatalogUids = machineCatalogService.getAllMachineCatalogUidsByUserId(userId);

        List<Machine> machines = PowerShellCommander.getAllMachinesOfUser(machineCatalogUids);

        return new ResponseEntity<>(machines, HttpStatus.OK);
    }

    /**
     * Post resource to perform a power action for the selected machine.
     * @param actionMap Containing the machine name on which machine the action should be performed and the action itself.
     * @return Returns a response containing the result of the action.
     */
    @PostMapping()
    public ResponseEntity<?> performPowerActionOnMachine(@PathVariable Long userId,
                                                         @RequestBody Map<String, String> actionMap)
    {
        String machineName = actionMap.get("machine_name");
        String action = actionMap.get("action");

        // TODO: FOR PROD
        PowerShellResponse response = PowerShellCommander.performPowerActionOnMachine(machineName, action);

        // TODO: FOR DEVELOP
//        PowerShellResponse response = new PowerShellResponse();
//        response.setResult(0);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * Post resource to delete selected machines.
     * @param userId ID to identify the current user.
     * @param rawMachineValue a JSON formatted string which contains the selected machines to delete.
     * @return Returns a response containing the result of the process.
     */
    @PostMapping("/deletes")
    public ResponseEntity<?> deleteSelectedMachines(@PathVariable Long userId, @RequestBody String rawMachineValue)
    {
        LOGGER.info("Incoming machine: " + rawMachineValue);
        // Extract machine names out of raw json string
        List<Machine> machines = JsonUtilities.convertRawJsonValueToMachinesList(rawMachineValue);

        // Extract machine names
        List<Integer> machineUids = machines.stream().map(Machine::getUid).collect(Collectors.toList());

        // Execute PowerShell process
        PowerShellResponse response = PowerShellCommander.deleteMachines(machineUids);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * Put resource to assign {@link com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user.ActiveDirectoryUser}s to
     * a selected machine.
     * @param userId ID to identify the current user.
     * @param rawMachineValue a JSON formatted string which contains the selected machines and the users which should be
     *                       assigned to the selected machine.
     * @return Returns a response containing the result of the process.
     */
    @PutMapping("/assign-users")
    public ResponseEntity<?> assignUsersToMachine(@PathVariable Long userId, @RequestBody String rawMachineValue)
    {
        LOGGER.info("assignUsersToMachine()");

        // Extract machines from incoming request
        List<Machine> machines = JsonUtilities.convertRawJsonValueToMachinesList(rawMachineValue);

        // Execute PowerShell process
        PowerShellResponse response = PowerShellCommander.addActiveDirectoryUsersToMachine(machines);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    // ----- GETTER & SETTER -----
    public DeliverGroupService getDeliverGroupService()
    {
        return deliverGroupService;
    }

    @Autowired
    public void setDeliverGroupService(DeliverGroupService deliverGroupService)
    {
        this.deliverGroupService = deliverGroupService;
    }

    public MachineCatalogService getMachineCatalogService()
    {
        return machineCatalogService;
    }

    @Autowired
    public void setMachineCatalogService(MachineCatalogService machineCatalogService)
    {
        this.machineCatalogService = machineCatalogService;
    }
}
