package com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.webservice_rs.citrix.machinecatalog;

import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user.User;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user.UserPropertyKey;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.machine.Machine;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.machinecatalog.MachineCatalog;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.service.citrix.MachineCatalogService;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.service.user.UserService;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.utilities.JsonUtilities;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.utilities.powershell.PowerShellCommander;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.utilities.powershell.PowerShellResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin()
@RequestMapping("/users/{userId}/web-services/machine-catalogs")
public class MachineCatalogServiceController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(MachineCatalogServiceController.class);

    protected UserService userService;
    protected MachineCatalogService machineCatalogService;

    /**
     * Constructor
     * @param userService Service to handle all necessary user operations
     * @param machineCatalogService Service to handle all necessary machine catalog operations
     */
    public MachineCatalogServiceController(UserService userService, MachineCatalogService machineCatalogService)
    {
        this.userService = userService;
        this.machineCatalogService = machineCatalogService;
    }

    /**
     * Handles the post request of creating a machine catalog.
     * @param rawMachineCatalogJson Raw JSON string which holds the data coming from the frontend.
     * @return Returns a response depending on the result of the machine catalog creation process.
     */
    @PostMapping()
    public ResponseEntity<?> createMachineCatalog(@PathVariable Long userId, @RequestBody String rawMachineCatalogJson)
    {
        LOGGER.info("Received machine catalog JSON");

        MachineCatalogDTO machineCatalogDTO = JsonUtilities.extractMachineCatalogJson(rawMachineCatalogJson);

        // Generate machine catalog out of incoming json
        MachineCatalog machineCatalog = generateMachineCatalog(userId, machineCatalogDTO);

        // Execute 'create-machine-catalog-script'
        PowerShellResponse response = PowerShellCommander.createNewMachineCatalog(machineCatalog);

        // If creating the machine catalog was successful, save it in the db
        if (response.getResult() != null && response.getResult() > 0)
        {
            LOGGER.info("Machine catalog was successfully created");

            // Set UID of the catalog
            machineCatalog.setUid(Long.valueOf(response.getResult()));

            // Save catalog information in the database
            MachineCatalog createdMachineCatalog = machineCatalogService.createAndSaveMachineCatalog(machineCatalog);

            // Check if saving was successful
            if (createdMachineCatalog == null)
            {
                LOGGER.error("Failed to save machine catalog");
                return new ResponseEntity<>("Failed to persist machine catalog after creation.",
                        HttpStatus.BAD_REQUEST);
            }
            LOGGER.info("Successfully saved machine catalog with ID: " + createdMachineCatalog.getUid());
            return new ResponseEntity<>(createdMachineCatalog, HttpStatus.OK);
        }
        else
        {
            // Return error response to frontend
            LOGGER.error("Creating machine catalog went wrong. PowerShellCommander returned: "
                    + response.getStatus().getStatusCode());
            return new ResponseEntity<>(response.getStatus().getDescription(), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get-resource for machine catalogs of specific user.
     * @param userId ID used to identify all machine catalogs of the current user.
     * @return Returns a response containing a list of {@link MachineCatalog}s of the current user.
     */
    @GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getMachineCatalogsOfUser(@PathVariable Long userId)
    {
        // Load machine catalogs of given user
        List<MachineCatalog> machineCatalogs = machineCatalogService.getAllMachineCatalogsByUserId(userId);
        return new ResponseEntity<>(machineCatalogs, HttpStatus.OK);
    }

    /**
     * Get-resource to retrieve all available (not user assigned) machines in a specific catalog
     * @param userId ID of the owner of the machine catalog.
     * @param machineCatalogUid UID of the requested machine catalog.
     * @return Returns a response containing a list of {@link Machine}s of the requested machine catalog.
     */
    @GetMapping(value="/{machineCatalogUid}/machines", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAvailableMachinesOfCatalog(@PathVariable Long userId,
                                                           @PathVariable Long machineCatalogUid)
    {
        // TODO: FOR PROD
        List<Machine> machines = PowerShellCommander.retrieveAllAvailableMachinesOfMachineCatalog(machineCatalogUid,
                false, false);

        // TODO: FOR DEVELOP
//        List<Machine> machines = new ArrayList<>();
//        for (int i = 1; i < 4; i++)
//        {
//            Machine machine = new Machine();
//            machine.setName("lawyer-00" + i);
//            machine.setPowerState(4);
//            machine.setPowerActions(Arrays.asList("Reset", "Restart", "Resume", "Shutdown", "Suspend", "TurnOff", "TurnOn"));
//
//            machines.add(machine);
//        }

        return new ResponseEntity<>(machines, HttpStatus.OK);
    }

    /**
     * Put resource to add new machines to a selected {@link MachineCatalog}.
     * @param userId
     * @param body Holds the data for the process.
     * @return Returns a {@link PowerShellResponse} containing the result of the process.
     */
    @PutMapping(value="/add-machines", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addMachinesToMachineCatalog(@PathVariable Long userId, @RequestBody Map<String, String> body)
    {
        String machineCatalogName = body.get("catalogName");
        int count = Integer.parseInt(body.get("count"));

        PowerShellResponse response = PowerShellCommander.addMachinesToMachineCatalog(machineCatalogName, count);

        if (response.getResult() > 0)
        {
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    // ----- HELPER METHODS -----
    /**
     * Extracts the data from the incoming json to generate a machine catalog.
     * @param userId ID of the user which the machine catalog should be assigned to.
     * @param json {@link MachineCatalogDTO} which holds all the data about the machine catalog to create.
     * @return Returns the newly generated machine catalog as a {@link MachineCatalog} object.
     */
    protected MachineCatalog generateMachineCatalog(Long userId, MachineCatalogDTO json)
    {
        // Load user
        User user = userService.getUserById(userId);

        // Generate Machine catalog object
        MachineCatalog catalog = new MachineCatalog();
        catalog.setUser(user);
        catalog.setOrganizationUnit(user.getPropertyValue(UserPropertyKey.AD_ORG_UNIT_COMPUTERS));
        catalog.setAccountNamingSchema(json.getAccountNamingSchema());
        catalog.setAccountNamingSchemaType(json.getAccountNamingSchemaType());
        catalog.setDescription(json.getDescription());
        catalog.setMemoryInMb(json.getMemoryInMb());
        catalog.setName(json.getName());
        catalog.setNumbersOfProcessors(json.getNumbersOfProcessors());
        catalog.setNumbersOfVms(json.getNumbersOfVms());

        return catalog;
    }


}
