package com.fhswf.kandziora.kevin.sspwebservicesbackend.utilities.powershell;

import com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.user_rs.ActiveDirectoryUserDTO;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user.ActiveDirectoryUser;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.deliverygroup.DeliveryGroup;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.machine.Machine;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.machinecatalog.MachineCatalog;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.utilities.JsonUtilities;
import org.apache.commons.lang3.BooleanUtils;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

@Configuration
@PropertySource("classpath:application.properties")
public class PowerShellCommander
{
    private static final Logger LOGGER = LoggerFactory.getLogger(PowerShellCommander.class);

    // --- PowerShell script parameters
    // Machine catalog
    protected static final String PARAM_CATALOG_NAME              = "-catalogName ";
    protected static final String PARAM_CATALOG_UID               = "-catalogUID ";
    protected static final String PARAM_DESCRIPTION               = "-description ";
    protected static final String PARAM_ORG_UNIT                  = "-orgUnit ";
    protected static final String PARAM_NAMING_SCHEME             = "-namingScheme ";
    protected static final String PARAM_NAMING_SCHEME_TYPE        = "-namingSchemeType ";
    protected static final String PARAM_VM_CPUS_COUNT             = "-vCpuCount ";
    protected static final String PARAM_VM_RAM                    = "-vRAM ";
    protected static final String PARAM_ACCOUNT_COUNT             = "-countOfAccounts ";
    protected static final String PARAM_ALL_MACHINES              = "-allMachines ";
    protected static final String PARAM_ASSIGNED_TO_DESKTOP_GROUP = "-assignedToDesktopGroup ";
    protected static final String PARAM_MACHINE_COUNT             = "-machineCount ";

    // Delivery group
    protected static final String PARAM_DELIVERY_GROUP_NAME    = "-deliveryGroupName ";
    protected static final String PARAM_USER_ASSIGNED_MACHINES = "-userAssignedMachines ";
    protected static final String PARAM_TURN_ON_ADDED_MACHINES = "-turnOnAddedMachines ";
    protected static final String PARAM_MACHINE_NAMES          = "-machineNames ";

    // Machines
    protected static final String PARAM_CATALOG_UIDS = "-catalogUIDs ";
    protected static final String PARAM_MACHINE_UID  = "-machineUid ";
    protected static final String PARAM_MACHINE_UIDS = "-machineUids ";

    // ActiveDirectory
    protected static final String PARAM_AD_USERS_OU_PATH = "-OUPath ";
    protected static final String PARAM_SIDS_TO_ASSIGN   = "-sidsToAssign ";


    // Power actions
    protected static final String PARAM_MACHINE_NAME = "-machineName ";
    protected static final String PARAM_ACTION_NAME  = "-action ";

    // --- PowerShell file paths
    protected static String PS_EXECUTION_PATH_MACOS;
    protected static String PS_EXECUTION_PATH_WIN;

    protected static final String PS_EXECUTABLE = "powershell.exe";

    // File names
    protected static String PS_CREATE_NEW_MACHINE_CATALOG_WITH_NEW_AD_COMPUTER_ACCOUNTS_FILE;
    protected static String PS_CREATE_NEW_DELIVERY_GROUP_FILE;
    protected static String PS_GET_USERS_OF_SPECIFIC_OU_FILE;
    protected static String PS_GET_ALL_MACHINES_OF_SPECIFIC_CATALOG_FILE;
    protected static String PS_GET_ALL_MACHINES_OF_USER_FILE;
    protected static String PS_EXECUTE_POWER_ACTION_ON_MACHINE_FILE;
    protected static String PS_ADD_MACHINES_TO_MACHINE_CATALOG_FILE;
    protected static String PS_DELETE_MACHINES_FILE;
    protected static String PS_ASSIGN_USERS_TO_MACHINE_FILE;
    protected static String PS_ADD_MACHINES_TO_DELIVERY_GROUP_FILE;

    /**
     * Creates a machine catalog based on the properties of the given entity. These properties will be the parameters
     * for the powershell-script which will be used to create the machine catalog.
     *
     * @param machineCatalog {@link MachineCatalog}
     * @return Returns a {@link PowerShellResponse} which contains the UID of the created machine catalog when the process
     * was successful, else it will contain an error code which is mapped in the {@link PowerShellStatus} class.
     */
    public static PowerShellResponse createNewMachineCatalog(MachineCatalog machineCatalog)
    {
        LOGGER.info("createNewMachineCatalog()");

        // Retrieve needed params for machine catalog creation and create param expression
        String params = createMachineCatalogParamExpression(machineCatalog);
        LOGGER.info("Machine catalog parameters:\n" + params);

        // Execute script
        int statusCode = executePowerShell(PS_CREATE_NEW_MACHINE_CATALOG_WITH_NEW_AD_COMPUTER_ACCOUNTS_FILE, params);

        // Identify status code
        return PowerShellStatus.getResponseByStatusCode(statusCode);
    }

    /**
     * Creates a delivery group based on the properties of the given entity. These properties will be the parameters
     * for the powershell-script which will be used to create the delivery group.
     *
     * @param deliveryGroup {@link DeliveryGroup}
     * @return Returns a {@link PowerShellResponse} which contains the UID of the created delivery group when the process
     * was successful, else it will contain an error code which is mapped in the {@link PowerShellStatus} class.
     */
    public static PowerShellResponse createNewDeliveryGroup(DeliveryGroup deliveryGroup)
    {
        LOGGER.info("createNewDeliveryGroup()");

        // Retrieve needed params for delivery group and create param expression
        String params = createDeliveryGroupParamExpression(deliveryGroup);
        LOGGER.info("Delivery group parameters:\n" + params);

        // Execute script
        int statusCode = executePowerShell(PS_CREATE_NEW_DELIVERY_GROUP_FILE, params);

        // Identify status code
        return PowerShellStatus.getResponseByStatusCode(statusCode);
    }

    /**
     *
     * Executes the PowerShell process with the given script path and the parameters
     * @param scriptPath Path to the script to execute
     * @param params Parameters for the script to execute
     * @return {@code true} if the execution was successful, else {@code false}.
     */
    protected static int executePowerShell(String scriptPath, String params)
    {
        // Status 0 means initial state and can occur
        int status = 0;

        // Build the command which should be executed
        String command = buildPowerShellCommand(scriptPath, params);
        LOGGER.info("Command to execute: " + command);

        // Executing the command
        Process powerShellProcess = null;
        try
        {
            powerShellProcess = Runtime.getRuntime().exec(command);

            // Getting the results
            powerShellProcess.getOutputStream().close();
            String line;
            // Log standard output
            LOGGER.info("Standard Output:");
            BufferedReader stdout = new BufferedReader(new InputStreamReader(powerShellProcess.getInputStream()));
            while ((line = stdout.readLine()) != null)
            {
                LOGGER.info(line);
                if (isNumeric(line))
                {
                    status = Integer.parseInt(line);
                }
            }
            stdout.close();

            // Log error output
            LOGGER.info("Standard Error:");
            BufferedReader stderr = new BufferedReader(new InputStreamReader(powerShellProcess.getErrorStream()));
            while ((line = stderr.readLine()) != null)
            {
                LOGGER.info(line);
                if (isNumeric(line))
                {
                    status = Integer.parseInt(line);
                }
            }
            stderr.close();

            LOGGER.info("Done");
            return status;
        }
        catch (IOException e)
        {
            LOGGER.error("Failed to run PowerShell, due to: " + e.getLocalizedMessage());
            status = PowerShellStatus.POWER_SHELL_INCORRECT_USE_FAILURE.getStatusCode();
        }
        finally
        {
            // Kill process if it is not null
            if (powerShellProcess != null)
            {
                powerShellProcess.destroy();
            }
        }

        return status;
    }

    /**
     * Builds the powershell command which will be executed in the java {@link Process}.
     * @param scriptPath Path of the script to execute.
     * @param params concatenated parameters for the script as a string.
     * @return Returns the powershell command as one string.
     */
    protected static String buildPowerShellCommand(String scriptPath, String params)
    {
        return PS_EXECUTABLE + " " + scriptPath + " " + params;
    }

    /**
     * Creates an expression for the parameter list needed for the machine catalog creation.
     * @param machineCatalog Specified {@link MachineCatalog} by the user from the UI.
     * @return Returns a list of parameters as a string expression.
     */
    protected static String createMachineCatalogParamExpression(MachineCatalog machineCatalog)
    {
        return PARAM_CATALOG_NAME + "'" + machineCatalog.getName() + "' "
                + PARAM_DESCRIPTION + "'" + machineCatalog.getDescription() + "' "
                + PARAM_NAMING_SCHEME + "'" + machineCatalog.getAccountNamingSchema() + "' "
                + PARAM_NAMING_SCHEME_TYPE + "'" + machineCatalog.getAccountNamingSchemaType() + "' "
                + PARAM_ORG_UNIT + "'" + machineCatalog.getOrganizationUnit() + "' "
                + PARAM_VM_CPUS_COUNT + machineCatalog.getNumbersOfProcessors() + " "
                + PARAM_VM_RAM + machineCatalog.getMemoryInMb() + " "
                + PARAM_ACCOUNT_COUNT + machineCatalog.getNumbersOfVms();
    }

    /**
     * Checks if the given parameter is a numeric value.
     * @param valueToCheck {@link String}
     * @return {@code true} if the given value is numeric, else {@code false}.
     */
    public static boolean isNumeric(String valueToCheck)
    {
        if (valueToCheck == null)
        {
            return false;
        }

        try
        {
            Integer.parseInt(valueToCheck);
        }
        catch (NumberFormatException e)
        {
            return false;
        }
        return true;
    }

    /**
     * Creates an expression for the parameter list needed for the delivery group creation.
     * @param deliveryGroup Specified {@link DeliveryGroup} by the user from the UI.
     * @return Returns a list of parameters as a string expression.
     */
    protected static String createDeliveryGroupParamExpression(DeliveryGroup deliveryGroup)
    {
        return PARAM_DELIVERY_GROUP_NAME + "'" + deliveryGroup.getName() + "' "
                + PARAM_DESCRIPTION + "'" + deliveryGroup.getDescription() + "' "
                + PARAM_CATALOG_UID + deliveryGroup.getCatalogUid() + " "
                + PARAM_USER_ASSIGNED_MACHINES + createUserAssignedMachinesMapParam(deliveryGroup) + " "
                + PARAM_TURN_ON_ADDED_MACHINES + BooleanUtils.toInteger(deliveryGroup.isTurnOnAddedMachines());
    }

    /**
     * Creates a PowerShell hashtable parameter for the 'create-new-delivery-group.ps1' script.
     * @param deliveryGroup Holds all the data needed for the parameter.
     * @return Returns a PowerShell hashtable as string.
     */
    protected static String createUserAssignedMachinesMapParam(DeliveryGroup deliveryGroup)
    {
         // Valid examples:
         // 1-machine-1-user: @{"<machine-name>"=@("<user-sid>")}
         // 1-machine-n-user: @{"<machine-name>"=@("<user-sid>"; "<user-sid>...")}
         // n-machine-1-user: @{"<machine-name>"=@("<user-sid>"); ...}
         // n-machine-n-user: @{"<machine-name>"=@("<user-sid>"; "<user-sid>..."); ...}

        String startOuterParam = "@{";
        String endOuterParam = "}";
        String startInnerParam = "=@(";
        String endInnerParam = ")";

        StringBuilder result = new StringBuilder(startOuterParam);

        for (int i = 0; i < deliveryGroup.getMachines().size(); i++)
        {
            result.append("'").append(deliveryGroup.getMachines().get(i).getName()).append("'").append(startInnerParam);

            StringJoiner userSidsJoiner = new StringJoiner(", ");
            for (String userSid : deliveryGroup.getMachines().get(i).getAssignedUsers().stream()
                    .map(ActiveDirectoryUserDTO::getSid).collect(Collectors.toList()))
            {
                userSidsJoiner.add("'" + userSid + "'");
            }

            result.append(userSidsJoiner.toString()).append(endInnerParam);

            if (i != (deliveryGroup.getMachines().size() - 1))
            {
                result.append("; ");
            }
        }

        result.append(endOuterParam);
        return result.toString();
    }

    /**
     * Executes a powershell process to obtain all ActiveDirectory users from a specific organization unit.
     * @param adOrgUnit The organization unit which should be requested.
     * @return Returns a list of {@link ActiveDirectoryUser}s of the requested organization unit.
     */
    public static List<ActiveDirectoryUser> retrieveActiveDirectoryUsersFromUser(String adOrgUnit)
    {
        // Build command
        String param = PARAM_AD_USERS_OU_PATH + "'" + adOrgUnit + "'";
        String command = buildPowerShellCommand(PS_GET_USERS_OF_SPECIFIC_OU_FILE, param);

        // Execute PowerShell script
        String powerShellResult = executePowerShellProcessWithJsonStringResult(command);

        // Convert PowerShell result and return it
        return JsonUtilities.convertPowerShellActiveDirectoryUsersResult(powerShellResult);
    }

    /**
     * Retrieves all available virtual machines of the specific machine catalog.
     * @param catalogUid UID of the machine catalog which will be requested.
     * @param allMachines Boolean to retrieve all machines no matter which assignment is present.
     * @param assignedToDesktopGroup Boolean to retrieve just the desktop group assigned machines or not assigned machines.
     * @return  Returns a list of {@link Machine}s of the requested machine catalog.
     */
    public static List<Machine> retrieveAllAvailableMachinesOfMachineCatalog(Long catalogUid, boolean allMachines,
                                                                             boolean assignedToDesktopGroup)
    {
        // Build command
        String param = createAllMachinesOfCatalogParamExpression(catalogUid, allMachines, assignedToDesktopGroup);
        String command = buildPowerShellCommand(PS_GET_ALL_MACHINES_OF_SPECIFIC_CATALOG_FILE, param);

        // Execute PowerShell script
        String powerShellResult = executePowerShellProcessWithJsonStringResult(command);

        // convert PowerShell result and return converted result
        return JsonUtilities.convertPowerShellMachinesResultToJson(powerShellResult);
    }

    /**
     * Executes PowerShell scripts which returns a JSON string.
     * @param command Command containing the script path and the needed parameters
     * @return Returns a JSON string which has to be converted afterwards.
     */
    public static String executePowerShellProcessWithJsonStringResult(String command)
    {
        String result = null;
        // Build powershell process
        Process powerShellProcess = null;
        try
        {
            powerShellProcess = Runtime.getRuntime().exec(command);

            // Getting the results
            powerShellProcess.getOutputStream().close();
            String line;
            // Log standard output
            LOGGER.info("Standard Output:");
            BufferedReader stdout = new BufferedReader(new InputStreamReader(powerShellProcess.getInputStream()));
            StringBuilder builder = new StringBuilder();
            while ((line = stdout.readLine()) != null)
            {
                LOGGER.info(line);
                builder.append(line);
            }
            stdout.close();

            // Log error output
            LOGGER.info("Standard Error:");
            BufferedReader stderr = new BufferedReader(new InputStreamReader(powerShellProcess.getErrorStream()));
            while ((line = stderr.readLine()) != null)
            {
                LOGGER.info(line);
            }
            stderr.close();

            LOGGER.info("Done");
            result = builder.toString();
        }
        catch (IOException e)
        {
            LOGGER.error("Failed to run PowerShell, due to: " + e.getLocalizedMessage());
        }
        catch (JSONException e)
        {
            LOGGER.error("Could not parse JSON: " + e.getMessage());
        }
        finally
        {
            // Kill process if it is not null
            if (powerShellProcess != null)
            {
                powerShellProcess.destroy();
            }
        }
        return result;
    }

    /**
     * Creates an expression for the parameter list needed for retrieving all machines of a specific catalog.
     * @param catalogUid UID of the catalog which will be requested.
     * @param allMachines If {@code true} all machines will be returned no matter if they are assigned to users or not.
     * @param assignedToDesktopGroup If {@code true} just machines which are already assigned to a delivery group will
     *                               be requested, else all machines without a delivery group will be returned.
     * @return Returns the parameter expression needed to retrieve the correct result of available machines.
     */
    protected static String createAllMachinesOfCatalogParamExpression(Long catalogUid, boolean allMachines,
                                                                      boolean assignedToDesktopGroup)
    {
        return PARAM_CATALOG_UID + catalogUid + " "
                + PARAM_ALL_MACHINES + BooleanUtils.toInteger(allMachines) + " "
                + PARAM_ASSIGNED_TO_DESKTOP_GROUP + BooleanUtils.toInteger(assignedToDesktopGroup);
    }

    /**
     * Obtains all machines of the user by the given delivery group UIDs.
     * @param machineCatalogUids UIDs of the delivery group to obtain all machines.
     * @return Returns a list of {@link Machine}s found by all delivery groups of the user.
     */
    public static List<Machine> getAllMachinesOfUser(List<Integer> machineCatalogUids)
    {
        // TODO: FOR PROD
        // Create param
        String param = generateParamForRetrievingAllMachinesOfUsers(machineCatalogUids);

        // Create command
        String command = buildPowerShellCommand(PS_GET_ALL_MACHINES_OF_USER_FILE, param);

        // Retrieve PowerShell result
        String rawJson = executePowerShellProcessWithJsonStringResult(command);

        // TODO: FOR DEVELOP
//        String rawJson = "[\n" + "    {\n" + "        \"DesktopGroupUid\": 56,\n" + "        \"HostedMachineName\": \"trainee-002\",\n" + "        \"CatalogName\": \"Trainees\",\n" + "        \"DesktopGroupName\": \"One machine\",\n" + "        \"AssociatedUserUPNs\": [\n" + "            \"jdoe@win2016s01.fhswf.de\",\n" + "            \"jschmitt@win2016s01.fhswf.de\"\n" + "        ],\n" + "        \"PowerState\": 4,\n" + "        \"MachineUid\": null,\n" + "        \"CatalogUid\": 64\n" + "    },\n" + "    {\n" + "        \"DesktopGroupUid\": 57,\n" + "        \"HostedMachineName\": \"trainee-003\",\n" + "        \"CatalogName\": \"Trainees\",\n" + "        \"DesktopGroupName\": \"One machine\",\n" + "        \"AssociatedUserUPNs\": [\n" + "        ],\n" + "        \"PowerState\": 7,\n" + "        \"MachineUid\": null,\n" + "        \"CatalogUid\": 64\n" + "    },\n" + "    {\n" + "        \"DesktopGroupUid\": 58,\n" + "        \"HostedMachineName\": \"trainee-004\",\n" + "        \"CatalogName\": \"Trainees\",\n" + "        \"DesktopGroupName\": \"One machine\",\n" + "        \"AssociatedUserUPNs\": [\n" + "            \"thiddle@win2016s01.fhswf.de\"\n" + "        ],\n" + "        \"PowerState\": 6,\n" + "        \"MachineUid\": null,\n" + "        \"CatalogUid\": 64\n" + "    }\n" + "]";

        // Generate
        return JsonUtilities.convertPowerShellMachinesResultToJson(rawJson);
    }

    /**
     * Generates the needed parameter for obtaining all machines of all delivery groups of the user.
     * @param catalogUids UIDs needed to create the parameter.
     * @return Returns the PowerShell parameter needed to obtain all machines of all delivery groups.
     */
    public static String generateParamForRetrievingAllMachinesOfUsers(List<Integer> catalogUids)
    {
        String catalogArray = buildArrayParamFromIntegerList(catalogUids);

        return PARAM_CATALOG_UIDS + catalogArray;
    }

    /**
     * Executes the given power action on the selected machine.
     * @param machineName Name of the machine on which the power action should be executed.
     * @param action Name of the action which should be executed.
     * @return Returns a {@link PowerShellResponse} with the result of the process.
     */
    public static PowerShellResponse performPowerActionOnMachine(String machineName, String action)
    {
        // Build params
        String params = PARAM_ACTION_NAME + "'" + action + "' " + PARAM_MACHINE_NAME + "'" + machineName + "'";

        // Build command
        String command = buildPowerShellCommand(PS_EXECUTE_POWER_ACTION_ON_MACHINE_FILE, params);

        // Execute command
        String rawJsonResult = executePowerShellProcessWithJsonStringResult(command);

        // Convert JSON result to a more readable value and return it
        return JsonUtilities.convertPowerActionExecutionResult(rawJsonResult, machineName);
    }

    /**
     * Executes power shell process to add new machines to a specific machine catalog.
     * @param machineCatalogName Name of the catalog to add new machines to.
     * @param count Amount of machines to be added.
     * @return Returns a {@link PowerShellResponse} containing the result of the process.
     */
    public static PowerShellResponse addMachinesToMachineCatalog(String machineCatalogName, int count)
    {
        // Build parameter list
        String params = PARAM_CATALOG_NAME + "'" + machineCatalogName + "' " + PARAM_MACHINE_COUNT
                + count;

        // Execute process
        int statusCode = executePowerShell(PS_ADD_MACHINES_TO_MACHINE_CATALOG_FILE, params);

        // Identify status code
        return PowerShellStatus.getResponseByStatusCode(statusCode);
    }

    /**
     * Executes power shell process to delete the selected machines.
     * @param machineUids a list of machine names to be deleted.
     * @return Returns a {@link PowerShellResponse} containing the result of the process.
     */
    public static PowerShellResponse deleteMachines(List<Integer> machineUids)
    {
        // Build parameter list
        String params = PARAM_MACHINE_UIDS + buildArrayParamFromIntegerList(machineUids);

        // Execute process
        int statusCode = executePowerShell(PS_DELETE_MACHINES_FILE, params);

        return PowerShellStatus.getResponseByStatusCode(statusCode);
    }

    /**
     * Executes power shell process to assign {@link ActiveDirectoryUser}s to given machine
     * @param machines Machine to assign selected users to.
     * @return Returns a {@link PowerShellResponse} containing the result of the process.
     */
    public static PowerShellResponse addActiveDirectoryUsersToMachine(List<Machine> machines)
    {
        // Extract parameter from given machine
        Machine machine = machines.get(0);
        int machineUid = machine.getUid();
        List<String> userSids = machine.getAssignedUsers().stream().map(ActiveDirectoryUser::getSid).collect(
                Collectors.toList());

        // Build parameter list
        String params = PARAM_MACHINE_UID + machineUid + " " + PARAM_SIDS_TO_ASSIGN + buildArrayParamFromStringList(userSids);

        // Execute process
        int statusCode = executePowerShell(PS_ASSIGN_USERS_TO_MACHINE_FILE, params);

        return PowerShellStatus.getResponseByStatusCode(statusCode);
    }

    /**
     * Executes power shell process to add the given list of machines to the selected delivery group.
     * @param machines Machines to add to the delivery group they are already holding.
     * @return Returns a {@link PowerShellResponse} containing the result of the process.
     */
    public static PowerShellResponse addMachinesToDeliveryGroup(List<Machine> machines)
    {
        PowerShellResponse response = new PowerShellResponse();

        // Extract parameters from given machines
        List<String> machineNames = machines.stream().map(Machine::getName).collect(Collectors.toList());
        List<String> deliveryGroupNames = machines.stream().map(Machine::getDeliveryGroupName).collect(Collectors.toList());

        // Distinct list of delivery group names
        List<String> distinctGroupNames = deliveryGroupNames.stream().distinct().collect(Collectors.toList());

        // List must not be greater than one, else selection in the frontend is buggy
        if (distinctGroupNames.size() > 1)
        {
            response.setResult(-7);
            response.setStatus(PowerShellStatus.TOO_MANY_DELIVERY_GROUPS_WHILE_ADDING);
            response.setMessage("Found too many delivery groups for adding machine(s)");

            return response;
        }

        // Build parameters
        String params = PARAM_DELIVERY_GROUP_NAME + "'" + deliveryGroupNames.get(0) + "' " + PARAM_MACHINE_NAMES
                + buildArrayParamFromStringList(machineNames);

        int statusCode = executePowerShell(PS_ADD_MACHINES_TO_DELIVERY_GROUP_FILE, params);
        LOGGER.info("Status of adding: " + statusCode);

        return PowerShellStatus.getResponseByStatusCode(statusCode);
    }

    // ----- HELPER METHODS -----
    /**
     * Help building array params for power shell scripts
     * @param values The values which should appear in the power shell array.
     * @return Returns the power shell array containing the given values.
     */
    protected static String buildArrayParamFromStringList(List<String> values)
    {
        StringJoiner stringJoiner = new StringJoiner(", ");
        for (String value : values)
        {
            stringJoiner.add("'" + value + "'");
        }

        return "@(" + stringJoiner.toString() + ")";
    }

    /**
     * Help building array params for power shell scripts
     * @param values The values which should appear in the power shell array.
     * @return Returns the power shell array containing the given values.
     */
    protected static String buildArrayParamFromIntegerList(List<Integer> values)
    {
        StringJoiner stringJoiner = new StringJoiner(", ");
        for (Integer value : values)
        {
            stringJoiner.add(String.valueOf(value));
        }

        return "@(" + stringJoiner.toString() + ")";
    }

    // ------ GETTERS & SETTERS ------
    @Value("${powershell.executionpath.macos}")
    public void setPsExecutionPathMacOs(String psExecutionPathMacOs)
    {
        PS_EXECUTION_PATH_MACOS = psExecutionPathMacOs;
    }

    @Value("${powershell.executionpath.windows}")
    public void setPsExecutionPathWin(String psExecutionPathWin)
    {
        PS_EXECUTION_PATH_WIN = psExecutionPathWin;
    }

    @Value("${powershell.script.filename.create-new-machine-catalog-with-new-ad-computer-accounts}")
    public void setPsCreateNewMachineCatalogWithNewAdComputerAccountsFileName(String psCreateNewMachineCatalogWithNewAdComputerAccountsFileName)
    {
        PS_CREATE_NEW_MACHINE_CATALOG_WITH_NEW_AD_COMPUTER_ACCOUNTS_FILE = psCreateNewMachineCatalogWithNewAdComputerAccountsFileName;
    }

    @Value("${powershell.script.filename.create-new-delivery-group}")
    public void setPsCreateNewDeliveryGroupFile(String psCreateNewDeliveryGroupFile)
    {
        PS_CREATE_NEW_DELIVERY_GROUP_FILE = psCreateNewDeliveryGroupFile;
    }

    @Value("${powershell.script.filename.get-all-users-of-specific-ou}")
    public void setPsGetUsersOfSpecificOuFile(String psGetUsersOfSpecificOuFile)
    {
        PS_GET_USERS_OF_SPECIFIC_OU_FILE = psGetUsersOfSpecificOuFile;
    }

    @Value("${powershell.script.filename.get-all-machines-of-specific-catalog}")
    public void setPsGetAllMachinesOfSpecificCatalogFile(String psGetAllMachinesOfSpecificCatalogFile)
    {
        PS_GET_ALL_MACHINES_OF_SPECIFIC_CATALOG_FILE = psGetAllMachinesOfSpecificCatalogFile;
    }

    @Value("${powershell.script.filename.get-all-machines-of-user}")
    public void setPsGetAllMachinesOfUserFile(String psGetAllMachinesOfUserFile)
    {
        PS_GET_ALL_MACHINES_OF_USER_FILE = psGetAllMachinesOfUserFile;
    }

    @Value("${powershell.script.filename.execute-power-action-on-machine}")
    public void setPsExecutePowerActionOnMachineFile(String psExecutePowerActionOnMachineFile)
    {
        PS_EXECUTE_POWER_ACTION_ON_MACHINE_FILE = psExecutePowerActionOnMachineFile;
    }

    @Value("${powershell.script.filename.add-new-machines-to-machine-catalog}")
    public void setPsAddMachinesToMachineCatalogFile(String psAddMachinesToMachineCatalogFile)
    {
        PS_ADD_MACHINES_TO_MACHINE_CATALOG_FILE = psAddMachinesToMachineCatalogFile;
    }

    @Value("${powershell.script.filename.delete-machines}")
    public void setPsDeleteMachinesFile(String psDeleteMachinesFile)
    {
        PS_DELETE_MACHINES_FILE = psDeleteMachinesFile;
    }

    @Value("${powershell.script.filename.assign-users-to-machine}")
    public void setPsAssignUsersToMachineFile(String psAssignUsersToMachineFile)
    {
        PS_ASSIGN_USERS_TO_MACHINE_FILE = psAssignUsersToMachineFile;
    }

    @Value("${powershell.script.filename.add-machines-to-delivery-group}")
    public void setPsAddMachinesToDeliveryGroupFile(String psAddMachinesToDeliveryGroupFile)
    {
        PS_ADD_MACHINES_TO_DELIVERY_GROUP_FILE = psAddMachinesToDeliveryGroupFile;
    }
}
