package com.fhswf.kandziora.kevin.sspwebservicesbackend.persistence.webservice.citrix;

import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.machinecatalog.MachineCatalog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MachineCatalogRepository extends JpaRepository<MachineCatalog, Long>
{
    @Query("SELECT mc FROM MachineCatalog mc JOIN FETCH mc.user u WHERE mc.user.id = ?1")
    List<MachineCatalog> findMachineCatalogsByUserId(Long id);

    @Query("SELECT mc.uid FROM MachineCatalog mc WHERE mc.user.id = ?1")
    List<Integer> findAllMachineCatalogUidsByUserId(Long id);
}
