package com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.machinecatalog;

import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user.User;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="machine_catalog")
public class MachineCatalog implements Serializable
{
    private static final long serialVersionUID = 736756045351082454L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="user_id", nullable=false)
    private User user;

    @Column(name="uid")
    private Long uid;

    @Column(name="name")
    private String name;

    @Transient
    private String description;

    @Transient
    private String organizationUnit;

    @Transient
    private Integer numbersOfVms;

    @Transient
    private Integer numbersOfProcessors;

    @Transient
    private Integer memoryInMb;

    @Transient
    private String accountNamingSchema;

    @Transient
    private NamingSchemaType accountNamingSchemaType;

    public MachineCatalog()
    {
        // intentionally empty
    }

    // ----- GETTER & SETTER -----

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    public Long getUid()
    {
        return uid;
    }

    public void setUid(Long uid)
    {
        this.uid = uid;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String machineCatalogName)
    {
        this.name = machineCatalogName;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getOrganizationUnit()
    {
        return organizationUnit;
    }

    public void setOrganizationUnit(String organizationUnit)
    {
        this.organizationUnit = organizationUnit;
    }

    public Integer getNumbersOfVms()
    {
        return numbersOfVms;
    }

    public void setNumbersOfVms(Integer numbersOfVms)
    {
        this.numbersOfVms = numbersOfVms;
    }

    public Integer getNumbersOfProcessors()
    {
        return numbersOfProcessors;
    }

    public void setNumbersOfProcessors(Integer numbersOfProcessors)
    {
        this.numbersOfProcessors = numbersOfProcessors;
    }

    public Integer getMemoryInMb()
    {
        return memoryInMb;
    }

    public void setMemoryInMb(Integer memoryInMb)
    {
        this.memoryInMb = memoryInMb;
    }

    public String getAccountNamingSchema()
    {
        return accountNamingSchema;
    }

    public void setAccountNamingSchema(String accountNamingSchema)
    {
        this.accountNamingSchema = accountNamingSchema;
    }

    public NamingSchemaType getAccountNamingSchemaType()
    {
        return accountNamingSchemaType;
    }

    public void setAccountNamingSchemaType(NamingSchemaType accountNamingSchemaType)
    {
        this.accountNamingSchemaType = accountNamingSchemaType;
    }
}
