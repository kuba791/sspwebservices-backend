package com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.webservice_rs.citrix.machinecatalog;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.machinecatalog.NamingSchemaType;

@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "uid")
public class MachineCatalogDTO
{
    private Long uid;
    private String name;
    private String description;
    private String organizationUnit;
    private Integer numbersOfVms;
    private Integer numbersOfProcessors;
    private Integer memoryInMb;
    private String accountNamingSchema;
    private NamingSchemaType accountNamingSchemaType;

    /**
     * Default constructor
     */
    public MachineCatalogDTO()
    {
        // intentionally empty
    }

    /**
     * Constructor
     * @param uid
     * @param name
     * @param description
     * @param organizationUnit
     * @param numbersOfVms
     * @param numbersOfProcessors
     * @param memoryInMb
     * @param accountNamingSchema
     * @param accountNamingSchemaType
     */
    public MachineCatalogDTO(Long uid, String name, String description, String organizationUnit, Integer numbersOfVms, Integer numbersOfProcessors, Integer memoryInMb, String accountNamingSchema, NamingSchemaType accountNamingSchemaType)
    {
        this.uid = uid;
        this.name = name;
        this.description = description;
        this.organizationUnit = organizationUnit;
        this.numbersOfVms = numbersOfVms;
        this.numbersOfProcessors = numbersOfProcessors;
        this.memoryInMb = memoryInMb;
        this.accountNamingSchema = accountNamingSchema;
        this.accountNamingSchemaType = accountNamingSchemaType;
    }

    // ----- GETTER & SETTER -----
    public Long getUid()
    {
        return uid;
    }

    public void setUid(Long uid)
    {
        this.uid = uid;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getOrganizationUnit()
    {
        return organizationUnit;
    }

    public void setOrganizationUnit(String organizationUnit)
    {
        this.organizationUnit = organizationUnit;
    }

    public Integer getNumbersOfVms()
    {
        return numbersOfVms;
    }

    public void setNumbersOfVms(Integer numbersOfVms)
    {
        this.numbersOfVms = numbersOfVms;
    }

    public Integer getNumbersOfProcessors()
    {
        return numbersOfProcessors;
    }

    public void setNumbersOfProcessors(Integer numbersOfProcessors)
    {
        this.numbersOfProcessors = numbersOfProcessors;
    }

    public Integer getMemoryInMb()
    {
        return memoryInMb;
    }

    public void setMemoryInMb(Integer memoryInMb)
    {
        this.memoryInMb = memoryInMb;
    }

    public String getAccountNamingSchema()
    {
        return accountNamingSchema;
    }

    public void setAccountNamingSchema(String accountNamingSchema)
    {
        this.accountNamingSchema = accountNamingSchema;
    }

    public NamingSchemaType getAccountNamingSchemaType()
    {
        return accountNamingSchemaType;
    }

    public void setAccountNamingSchemaType(NamingSchemaType accountNamingSchemaType)
    {
        this.accountNamingSchemaType = accountNamingSchemaType;
    }
}
