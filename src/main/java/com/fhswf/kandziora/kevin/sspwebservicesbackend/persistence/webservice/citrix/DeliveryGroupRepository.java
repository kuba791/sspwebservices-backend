package com.fhswf.kandziora.kevin.sspwebservicesbackend.persistence.webservice.citrix;

import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.deliverygroup.DeliveryGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeliveryGroupRepository extends JpaRepository<DeliveryGroup, Long>
{
    @Query("SELECT dg FROM DeliveryGroup dg JOIN FETCH dg.user u WHERE dg.user.id = ?1")
    List<DeliveryGroup> findDeliveryGroupsByUserId(Long id);

    @Query("SELECT dg.uid FROM DeliveryGroup dg WHERE dg.user.id = ?1")
    List<String> findAllDeliveryGroupUidsByUserId(Long id);
}
