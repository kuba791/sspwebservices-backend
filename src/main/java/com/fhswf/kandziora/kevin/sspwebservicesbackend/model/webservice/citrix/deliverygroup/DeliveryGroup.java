package com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.deliverygroup;

import com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.webservice_rs.citrix.machine.MachineDTO;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name="delivery_group")
public class DeliveryGroup implements Serializable
{
    private static final long serialVersionUID = -8582677476186452356L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="user_id", nullable=false)
    private User user;

    @Column(name="uid")
    private Long uid;

    @Column(name="name")
    private String name;

    @Column(name="description")
    private String description;

    @Transient
    private Long catalogUid;

    @Transient
    private List<MachineDTO> machines;

    @Transient
    private boolean turnOnAddedMachines;

    /**
     * Constructor
     */
    public DeliveryGroup()
    {
        // intentionally empty
    }

    // ----- GETTER & SETTER -----
    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    public Long getUid()
    {
        return uid;
    }

    public void setUid(Long uid)
    {
        this.uid = uid;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Long getCatalogUid()
    {
        return catalogUid;
    }

    public void setCatalogUid(Long catalogUid)
    {
        this.catalogUid = catalogUid;
    }

    public List<MachineDTO> getMachines()
    {
        return machines;
    }

    public void setMachines(List<MachineDTO> machines)
    {
        this.machines = machines;
    }

    public boolean isTurnOnAddedMachines()
    {
        return turnOnAddedMachines;
    }

    public void setTurnOnAddedMachines(boolean turnOnAddedMachines)
    {
        this.turnOnAddedMachines = turnOnAddedMachines;
    }
}
