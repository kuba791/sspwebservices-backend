package com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.user_rs;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.io.Serializable;

@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "sid")
public class ActiveDirectoryUserDTO implements Serializable
{
    private static final long serialVersionUID = 7858963090358426745L;

    private String name;
    private String userPrincipalName;
    private String objectGuid;
    private String sid;

    /**
     * Empty constructor
     */
    public ActiveDirectoryUserDTO()
    {
        // intentionally empty
    }

    /**
     * Constructor
     * @param name Name of the AD user
     * @param userPrincipalName principal name of the AD user
     * @param objectGuid Object GUID of the AD user
     * @param sid SID of the AD user
     */
    @JsonCreator
    public ActiveDirectoryUserDTO(@JsonProperty String name, @JsonProperty String userPrincipalName,
                                  @JsonProperty String objectGuid, @JsonProperty String sid)
    {
        this.name = name;
        this.userPrincipalName = userPrincipalName;
        this.objectGuid = objectGuid;
        this.sid = sid;
    }

    // ----- GETTER & SETTER -----
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getUserPrincipalName()
    {
        return userPrincipalName;
    }

    public void setUserPrincipalName(String userPrincipalName)
    {
        this.userPrincipalName = userPrincipalName;
    }

    public String getObjectGuid()
    {
        return objectGuid;
    }

    public void setObjectGuid(String objectGuid)
    {
        this.objectGuid = objectGuid;
    }

    public String getSid()
    {
        return sid;
    }

    public void setSid(String sid)
    {
        this.sid = sid;
    }
}
