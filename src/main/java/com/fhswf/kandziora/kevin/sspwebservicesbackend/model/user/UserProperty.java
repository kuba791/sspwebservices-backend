package com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user;

import javax.persistence.*;

/**
 * Class representing diverse properties of a specific {@link User}.
 */
@Entity
@Table(name="user_property")
public class UserProperty
{
    @EmbeddedId
    private UserPropertyPK id;

    @Column(name="value")
    private String value;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="user_id", nullable=false, updatable=false)
    @MapsId("userId")
    private User user;

    // ----- GETTER & SETTER -----

    public String getPropertyKey()
    {
        if (id == null)
        {
            return null;
        }
        return id.getPropertyKey();
    }

    public void setPropertyKey(String propertyKey)
    {
        if (id == null)
        {
            id = new UserPropertyPK();
        }
        id.setPropertyKey(propertyKey);
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }
}
