package com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape=JsonFormat.Shape.OBJECT)
public enum XenDesktopConfigurationType
{
    XENDESKTOP_MACHINE_CATALOG("xendesktop_machine_catalog", "XenDesktop machine catalog", "Creates a machine catalog for virtual machines."),
    XENDESKTOP_DELIVERY_GROUP("xendesktop_delivery_group", "XenDesktop delivery group", "A Delivery group is a collection of machines selected from one or more machine catalogs. The Delivery group specifies which users can use those machines.<br><br><b>Note:</b> Before creating a delivery group, you need to create at least one machine catalog.");

    private String type;
    private String name;
    private String description;

    XenDesktopConfigurationType(String type, String name, String description)
    {
        this.type = type;
        this.name = name;
        this.description = description;
    }

    // ----- GETTERS & SETTERS -----

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
}
