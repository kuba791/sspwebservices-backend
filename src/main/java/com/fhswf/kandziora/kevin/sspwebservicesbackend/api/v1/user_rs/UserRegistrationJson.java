package com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.user_rs;

public class UserRegistrationJson
{
    private String userMail;
    private String userPassword;

    public UserRegistrationJson(String userMail, String userPassword)
    {
        this.userMail = userMail;
        this.userPassword = userPassword;
    }

    // ----- GETTERS & SETTERS ------

    public String getUserMail()
    {
        return userMail;
    }

    public void setUserMail(String userMail)
    {
        this.userMail = userMail;
    }

    public String getUserPassword()
    {
        return userPassword;
    }

    public void setUserPassword(String userPassword)
    {
        this.userPassword = userPassword;
    }
}
