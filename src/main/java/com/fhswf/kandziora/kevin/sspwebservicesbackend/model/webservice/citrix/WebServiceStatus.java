package com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix;

@Deprecated
public enum WebServiceStatus
{
    IN_PROGRESS("inProgress"),
    READY("ready"),
    ACTIVE("active"),
    DEACTIVATED("deactivated");

    private String status;

    /**
     * Constructor
     * @param status
     */
    WebServiceStatus(String status)
    {
        this.status = status;
    }

    // ----- GETTERS & SETTERS -----

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }
}
