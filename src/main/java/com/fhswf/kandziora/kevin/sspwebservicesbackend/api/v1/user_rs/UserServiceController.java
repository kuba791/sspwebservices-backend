package com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.user_rs;

import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user.ActiveDirectoryUser;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user.User;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user.UserProperty;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user.UserPropertyKey;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.service.citrix.MachineCatalogService;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.service.user.UserPrincipal;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.service.user.UserService;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.utilities.powershell.PowerShellCommander;
import org.hibernate.ObjectNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Resource class for all {@link User} actions.
 */
@RestController
@CrossOrigin()
@RequestMapping("/users")
public class UserServiceController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceController.class);

    private static final String AD_USERS_JSON = "[\n" + " {\n" + "\t \"Name\":  \"Johanna Schmitt\",\n" + "\t \"UserPrincipalName\":  \"jschmitt@win2016s01.fhswf.de\",\n" + "\t \"ObjectGUID\":  \"181f639e-c1cd-429b-8435-f47b98616c20\",\n" + "\t \"SID\":  {\n" + "\t\t\t\t \"BinaryLength\":  28,\n" + "\t\t\t\t \"AccountDomainSid\":  \"S-1-5-21-2649842711-4096779769-2815699909\",\n" + "\t\t\t\t \"Value\":  \"S-1-5-21-2649842711-4096779769-2815699909-1656\"\n" + "\t\t\t }\n" + " },\n" + " {\n" + "\t \"Name\":  \"John Doe\",\n" + "\t \"UserPrincipalName\":  \"jdoe@win2016s01.fhswf.de\",\n" + "\t \"ObjectGUID\":  \"43b5da30-2370-414d-9213-888d47e89231\",\n" + "\t \"SID\":  {\n" + "\t\t\t\t \"BinaryLength\":  28,\n" + "\t\t\t\t \"AccountDomainSid\":  \"S-1-5-21-2649842711-4096779769-2815699909\",\n" + "\t\t\t\t \"Value\":  \"S-1-5-21-2649842711-4096779769-2815699909-1657\"\n" + "\t\t\t }\n" + " },\n" + " {\n" + "\t \"Name\":  \"Tom Hiddleston\",\n" + "\t \"UserPrincipalName\":  \"thiddle@win2016s01.fhswf.de\",\n" + "\t \"ObjectGUID\":  \"31b128a2-fab1-4f5a-92ee-196229585687\",\n" + "\t \"SID\":  {\n" + "\t\t\t\t \"BinaryLength\":  28,\n" + "\t\t\t\t \"AccountDomainSid\":  \"S-1-5-21-2649842711-4096779769-2815699909\",\n" + "\t\t\t\t \"Value\":  \"S-1-5-21-2649842711-4096779769-2815699909-1658\"\n" + "\t\t\t }\n" + " }\n" + "]";

    protected UserService userService;
    protected MachineCatalogService machineCatalogService;

    /**
     * Authenticates the incoming {@link User} logging in to the Self-Service Portal.
     *
     * @param principal {@link Principal} - Decoratored
     * {@link com.fhswf.kandziora.kevin.sspwebservicesbackend.service.user.UserPrincipal} which contains the
     * {@link User}.
     *
     * @return Returns a {@link ResponseEntity} with the found {@link User} and {@link HttpStatus} 200, else it returns
     * {@link HttpStatus} 401.
     */
    @GetMapping("/authenticate")
    public ResponseEntity<?> retrievePrincipal(Principal principal)
    {
        LOGGER.info("retrievePrincipal()");

        // Check if user could be found
        if (principal != null)
        {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

            // Check if user is authenticated
            if (!(authentication instanceof AnonymousAuthenticationToken))
            {
                LOGGER.info("Incoming user is authenticated: " + authentication.isAuthenticated());
                Long userId = null;

                if (authentication.getPrincipal() instanceof UserPrincipal)
                {
                    userId = ((UserPrincipal) authentication.getPrincipal()).getId();
                }
                User user = userService.getUserById(userId);

                // Map user principal object to user object and return it
                return new ResponseEntity<>(user, HttpStatus.OK);
            } else
            {
                LOGGER.error("User could not be authenticated.");
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else
        {
            LOGGER.info("User was not found.");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Registers a new {@link User} and stores it in the DB.
     *
     * @return Returns a {@link ResponseEntity} with the found {@link User} and {@link HttpStatus} 200, else it returns
     * {@link HttpStatus} 400, if the user creation failed.
     */
    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@RequestBody UserRegistrationJson userJson)
    {
        LOGGER.info("registerUser()");

        User user = userService.createAndSaveUser(userJson);

        if (user == null)
        {
            LOGGER.error("Failed to create user");
            return new ResponseEntity<>("Failed to persist user while registration.",
                    HttpStatus.BAD_REQUEST);
        }
        LOGGER.info("Successfully registered user: " + user.getMail());
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping(value="/{userId}/properties", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getUserProperties(@PathVariable String userId)
    {
       LOGGER.info("getUserProperties()");

       // Need to cast from string to long
       Long currentUserId = Long.valueOf(userId);

       // Retrieve all user properties of current user
       List<UserProperty> userProperties = userService.getUserPropertiesByUserId(currentUserId);

       UserPropertiesJson userPropertiesJson = null;
       if (!CollectionUtils.isEmpty(userProperties))
       {
           // Map user properties from list to map
           Map<String, String> properties = userProperties.stream().collect(
                   Collectors.toMap(UserProperty::getPropertyKey, UserProperty::getValue));

           // Create json for response
           userPropertiesJson = new UserPropertiesJson(currentUserId, properties);
       }

       return new ResponseEntity<>(userPropertiesJson, HttpStatus.OK);
    }

    /**
     * Saves and updates {@link com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user.UserProperty} of a specific
     * {@link User}.
     *
     * @param propertiesJson {@link UserPropertiesJson} JSON object containing a map of {@link UserProperty} with the
     * corresponding user ID.
     * @return Returns the current {@link User} if saving was successful, else the response is BAD_REQUEST.
     */
    @PostMapping("/properties")
    public ResponseEntity<?> saveUserProperties(@RequestBody UserPropertiesJson propertiesJson)
    {
        LOGGER.info("saveUserProperties()");

        // Get user by id coming from JSON
        Long userId = propertiesJson.userId;
        User currentUser = null;
        try
        {
            currentUser = userService.getUserById(userId);
        }
        catch (ObjectNotFoundException e)
        {
            LOGGER.error(e.getMessage());
            return new ResponseEntity<>("The requested user does not exist.", HttpStatus.BAD_REQUEST);
        }

        // Extract user properties from JSON and assign them to the user
        assignUserPropertiesToUser(currentUser, propertiesJson);

        // Save/update the user
        currentUser = userService.saveUser(currentUser);

        if (currentUser == null)
        {
            LOGGER.error("Failed to save users settings");
            return new ResponseEntity<>("Failed to save users settings.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        LOGGER.info("Successfully saved user settings");
        return new ResponseEntity<>(currentUser, HttpStatus.OK);
    }

    /**
     * Get-resource for active directory users of specific user.
     * @param userId ID used to identify the right organization unit of the current user.
     * @return Returns a response containing a list of {@link ActiveDirectoryUser}s with all active directory user of the
     * current user.
     */
    @GetMapping(value = "/{userId}/active-directory-users", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getActiveDirectoryUsersFromUser(@PathVariable Long userId)
    {
        // Load user
        User user = userService.getUserById(userId);

        String adOrgUnitUsers = user.getPropertyValue(UserPropertyKey.AD_ORG_UNIT_USERS) ;

        // Retrieve AD users from incoming user as JSON
        // TODO: FOR PROD
        List<ActiveDirectoryUser> activeDirectoryUsers = PowerShellCommander.retrieveActiveDirectoryUsersFromUser(adOrgUnitUsers);
        // TODO: FOR DEVELOP
//        List<ActiveDirectoryUser> activeDirectoryUsers = JsonUtilities.convertPowerShellActiveDirectoryUsersResult(AD_USERS_JSON);

        return new ResponseEntity<>(activeDirectoryUsers, HttpStatus.OK);
    }

    // ----- OTHER METHODS -----
    /**
     * Assigns all the properties coming from the JSON to the corresponding {@link User}.
     *
     * @param user {@link User}
     * @param propertiesJson {@link UserPropertiesJson}
     */
    protected void assignUserPropertiesToUser(User user, UserPropertiesJson propertiesJson)
    {
        for (Map.Entry<String, String> propertyEntry : propertiesJson.properties.entrySet())
        {
            user.addProperty(propertyEntry.getKey(), propertyEntry.getValue());
        }
    }

    // ----- GETTERS & SETTERS -----

    public UserService getUserService()
    {
        return userService;
    }

    @Autowired
    public void setUserService(UserService userService)
    {
        this.userService = userService;
    }

    public MachineCatalogService getMachineCatalogService()
    {
        return machineCatalogService;
    }

    @Autowired
    public void setMachineCatalogService(MachineCatalogService machineCatalogService)
    {
        this.machineCatalogService = machineCatalogService;
    }
}
