package com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.machine;

import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user.ActiveDirectoryUser;

import java.io.Serializable;
import java.util.List;

public class Machine implements Serializable
{
    private static final long serialVersionUID = 2380837295685588693L;

    private int uid;
    private String name;
    private int catalogUid;
    private String catalogName;
    private String deliveryGroupName;
    private List<ActiveDirectoryUser> assignedUsers;
    private int powerState;
    private List<String> powerActions;

    /**
     * Empty Constructor
     */
    public Machine()
    {
        // Intentionally empty
    }

    /**
     * Constructor
     * @param uid
     * @param name
     * @param catalogUid
     * @param catalogName
     * @param deliveryGroupName
     * @param assignedUsers
     * @param powerState
     * @param powerActions
     */
    public Machine(int uid, String name, int catalogUid, String catalogName, String deliveryGroupName, List<ActiveDirectoryUser> assignedUsers, int powerState, List<String> powerActions)
    {
        this.uid = uid;
        this.name = name;
        this.catalogUid = catalogUid;
        this.catalogName = catalogName;
        this.deliveryGroupName = deliveryGroupName;
        this.assignedUsers = assignedUsers;
        this.powerState = powerState;
        this.powerActions = powerActions;
    }

    // ----- GETTER & SETTER -----
    public int getUid()
    {
        return uid;
    }

    public void setUid(int uid)
    {
        this.uid = uid;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getCatalogUid()
    {
        return catalogUid;
    }

    public void setCatalogUid(int catalogUid)
    {
        this.catalogUid = catalogUid;
    }

    public String getCatalogName()
    {
        return catalogName;
    }

    public void setCatalogName(String catalogName)
    {
        this.catalogName = catalogName;
    }

    public String getDeliveryGroupName()
    {
        return deliveryGroupName;
    }

    public void setDeliveryGroupName(String deliveryGroupName)
    {
        this.deliveryGroupName = deliveryGroupName;
    }

    public List<ActiveDirectoryUser> getAssignedUsers()
    {
        return assignedUsers;
    }

    public void setAssignedUsers(List<ActiveDirectoryUser> assignedUsers)
    {
        this.assignedUsers = assignedUsers;
    }

    public int getPowerState()
    {
        return powerState;
    }

    public void setPowerState(int powerState)
    {
        this.powerState = powerState;
    }

    public List<String> getPowerActions()
    {
        return powerActions;
    }

    public void setPowerActions(List<String> powerActions)
    {
        this.powerActions = powerActions;
    }
}
