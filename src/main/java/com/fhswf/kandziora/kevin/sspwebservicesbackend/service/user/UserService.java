package com.fhswf.kandziora.kevin.sspwebservicesbackend.service.user;

import com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.user_rs.UserRegistrationJson;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user.User;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user.UserProperty;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.persistence.user.UserRepository;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.utilities.JsonUtilities;
import org.hibernate.ObjectNotFoundException;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * This class is for interacting with the corresponding {@link UserRepository} for specific {@link User} actions,
 * such as create, findByName, etc.
 */
@Service
public class UserService implements UserDetailsService
{
    private static final Logger LOGGER = Logger.getLogger(UserService.class);

    protected UserRepository userRepository;
    protected PasswordEncoder passwordEncoder;

    /**
     * Loads the incoming user by its mail address.
     * @param mail {@link String} - Mail address of incoming user.
     * @return Returns a {@link UserPrincipal} object of the incoming user, if the user exists.
     */
    @Override
    public UserDetails loadUserByUsername(String mail)
    {
        User user = getUserByMail(mail);
        if (user != null)
        {
            return new UserPrincipal(user);
        }
        else
        {
            return null;
        }
    }

    /**
     * Creates and saves the retrieved {@link User} from the {@link UserRegistrationJson}.
     * @param userJson {@link UserRegistrationJson} - JSON formatted data coming from the frontend
     * @return Returns the created and persisted {@link User}, if nothing went wrong, else {@code null}.
     */
    public User createAndSaveUser(UserRegistrationJson userJson)
    {
        // Creates user from the JSON coming from the frontend
        User user = JsonUtilities.createUserFromJson(userJson);
        // Encrypt the users password
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        // Persist new user in db
        return saveUser(user);
    }

    /**
     * Saves the given {@link User}.
     * @param user {@link User}
     * @return Returns the persisted {@link User} when saving was successful, else {@code null}.
     */
    public User saveUser(User user)
    {
        try
        {
            user = userRepository.save(user);
            return user;
        }
        catch (Exception e)
        {
            LOGGER.error("Failed to save/update user: " + e.getMessage());
        }
        return null;
    }

    /**
     * Returns the {@link User} from the DB searched by its mail address.
     * @param mail {@link String} - Mail address from searched {@link User}
     * @return Returns the corresponding {@link User}, else {@code null}.
     */
    public User getUserByMail(String mail)
    {
        Optional<User> userOptional = userRepository.findByMail(mail);

        if (userOptional.isPresent())
        {
            return userOptional.get();
        }
        else
        {
            LOGGER.error("User with e-mail address: " + mail + " not found");
            return null;
        }
    }

    /**
     * Returns the {@link User} from the DB searched by its ID.
     * @param id {@link Long} - ID of the searched {@link User}.
     * @return Returns the corresponding {@link User}, else throws {@link ObjectNotFoundException}.
     * @throws ObjectNotFoundException in case that the {@link User} could not be found by the given ID.
     */
    public User getUserById(Long id) throws ObjectNotFoundException
    {
        Optional<User> userOptional = userRepository.findById(id);

        if (userOptional.isEmpty())
        {
            throw new ObjectNotFoundException("Not found", "User with ID: " + id + "could not be found.");
        }

        return userOptional.get();
    }

    /**
     * Delivers all user properties of the given user.
     * @param userId ID of the user.
     * @return Returns a list of {@link UserProperty}s of the given user.
     */
    public List<UserProperty> getUserPropertiesByUserId(Long userId)
    {
        return userRepository.findUserPropertiesByUserId(userId);
    }

    // ----- GETTERS & SETTERS -----

    public UserRepository getUserDao()
    {
        return userRepository;
    }

    @Autowired
    public void setUserDao(UserRepository userRepository)
    {
        this.userRepository = userRepository;
    }

    public PasswordEncoder getPasswordEncoder()
    {
        return passwordEncoder;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder)
    {
        this.passwordEncoder = passwordEncoder;
    }
}
