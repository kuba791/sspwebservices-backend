package com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.webservice_rs.citrix.machine;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.user_rs.ActiveDirectoryUserDTO;

import java.util.List;

@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "uid")
public class MachineDTO
{
    private Long uid;
    private String name;
    private String catalogName;
    private Long catalogUid;
    private List<ActiveDirectoryUserDTO> assignedUsers;
    private int powerState;
    private List<String> powerActions;

    /**
     * Default constructor
     */
    public MachineDTO()
    {
        // intentionally empty
    }

    /**
     * Constructor
     * @param uid
     * @param name
     * @param catalogName
     * @param catalogUid
     * @param assignedUsers
     * @param powerState
     * @param powerActions
     */
    public MachineDTO(Long uid, String name, String catalogName, Long catalogUid, List<ActiveDirectoryUserDTO> assignedUsers, int powerState, List<String> powerActions)
    {
        this.uid = uid;
        this.name = name;
        this.catalogName = catalogName;
        this.catalogUid = catalogUid;
        this.assignedUsers = assignedUsers;
        this.powerState = powerState;
        this.powerActions = powerActions;
    }

    // ----- GETTER & SETTER -----
    public Long getUid()
    {
        return uid;
    }

    public void setUid(Long uid)
    {
        this.uid = uid;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getCatalogName()
    {
        return catalogName;
    }

    public void setCatalogName(String catalogName)
    {
        this.catalogName = catalogName;
    }

    public Long getCatalogUid()
    {
        return catalogUid;
    }

    public void setCatalogUid(Long catalogUid)
    {
        this.catalogUid = catalogUid;
    }

    public List<ActiveDirectoryUserDTO> getAssignedUsers()
    {
        return assignedUsers;
    }

    public void setAssignedUsers(List<ActiveDirectoryUserDTO> assignedUsers)
    {
        this.assignedUsers = assignedUsers;
    }

    public int getPowerState()
    {
        return powerState;
    }

    public void setPowerState(int powerState)
    {
        this.powerState = powerState;
    }

    public List<String> getPowerActions()
    {
        return powerActions;
    }

    public void setPowerActions(List<String> powerActions)
    {
        this.powerActions = powerActions;
    }
}
