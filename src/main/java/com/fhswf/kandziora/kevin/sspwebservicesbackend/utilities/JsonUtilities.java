package com.fhswf.kandziora.kevin.sspwebservicesbackend.utilities;

import com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.user_rs.ActiveDirectoryUserDTO;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.user_rs.UserRegistrationJson;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.webservice_rs.citrix.deliverygroup.DeliveryGroupDTO;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.webservice_rs.citrix.machine.MachineDTO;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.webservice_rs.citrix.machinecatalog.MachineCatalogDTO;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user.ActiveDirectoryUser;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user.User;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user.UserRole;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.machine.Machine;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.machinecatalog.MachineCatalog;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.machinecatalog.NamingSchemaType;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.utilities.powershell.PowerShellResponse;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.utilities.powershell.PowerShellStatus;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class for user interactions.
 */
public class JsonUtilities
{
    // JSON properties
    // ActiveDirectory users
    protected static final String POWERSHELL_AD_USER_JSON_VALUE_NAME                = "Name";
    protected static final String POWERSHELL_AD_USER_JSON_VALUE_USER_PRINCIPAL_NAME = "UserPrincipalName";
    protected static final String POWERSHELL_AD_USER_JSON_VALUE_OBJECT_GUID         = "ObjectGUID";
    protected static final String POWERSHELL_AD_USER_JSON_VALUE_SID                 = "SID";
    protected static final String POWERSHELL_AD_USER_JSON_VALUE_SID_VALUE           = "Value";

    protected static final String AD_USER_JSON_VALUE_NAME                = "name";
    protected static final String AD_USER_JSON_VALUE_USER_PRINCIPAL_NAME = "userPrincipalName";
    protected static final String AD_USER_JSON_VALUE_OBJECT_GUID         = "objectGuid";
    protected static final String AD_USER_JSON_VALUE_SID                 = "sid";

    // Machine of machine catalogs
    protected static final String POWERSHELL_MC_MACHINE_JSON_VALUE_UID                  = "Uid";
    protected static final String POWERSHELL_MC_MACHINE_JSON_VALUE_NAME                 = "HostedMachineName";
    protected static final String POWERSHELL_MC_MACHINE_JSON_VALUE_MACHINE_CATALOG_NAME = "CatalogName";
    protected static final String POWERSHELL_MC_MACHINE_JSON_VALUE_MACHINE_CATALOG_UID  = "CatalogUID";
    protected static final String POWERSHELL_MC_MACHINE_JSON_VALUE_DELIVERY_GROUP_NAME  = "DesktopGroupName";
    protected static final String POWERSHELL_MC_MACHINE_JSON_VALUE_ASSIGNED_USERS       = "AssociatedUserUPNs";
    protected static final String POWERSHELL_MC_MACHINE_JSON_VALUE_POWER_STATE          = "PowerState";
    protected static final String POWERSHELL_MC_MACHINE_JSON_VALUE_POWER_ACTIONS        = "SupportedPowerActions";

    protected static final String MC_MACHINE_JSON_VALUE_MACHINES            = "machines";
    protected static final String MC_MACHINE_JSON_VALUE_UID                 = "uid";
    protected static final String MC_MACHINE_JSON_VALUE_NAME                = "name";
    protected static final String MC_MACHINE_JSON_VALUE_CATALOG_UID         = "catalogUid";
    protected static final String MC_MACHINE_JSON_VALUE_CATALOG_NAME        = "catalogName";
    protected static final String MC_MACHINE_JSON_VALUE_DELIVERY_GROUP_NAME = "deliveryGroupName";
    protected static final String MC_MACHINE_JSON_VALUE_ASSIGNED_USERS      = "assignedUsers";
    protected static final String MC_MACHINE_JSON_VALUE_POWER_STATE         = "powerState";
    protected static final String MC_MACHINE_JSON_VALUE_POWER_ACTIONS       = "powerActions";


    /**
     * Creates a {@link User} from the information of the given {@link UserRegistrationJson}.
     * @param userJson {@link UserRegistrationJson} - JSON formatted user information coming from the frontend.
     * @return Returns a {@link User} from the given {@link UserRegistrationJson}.
     */
    public static User createUserFromJson(UserRegistrationJson userJson)
    {
        User user = new User();
        user.setMail(userJson.getUserMail());
        user.setPassword(userJson.getUserPassword());
        user.setActive(true);
        user.setUserRole(UserRole.ADMIN);

        return user;
    }

    /**
     * Extracts the data from the incoming json to generate a machine catalog.
     * @param json {@link MachineCatalogDTO} which holds all the data about the machine catalog to create.
     * @return Returns the newly generated machine catalog as a {@link MachineCatalog} object.
     */
    public static MachineCatalogDTO extractMachineCatalogJson(String json)
    {
        MachineCatalogDTO catalogJson = new MachineCatalogDTO();
        JSONObject jsonObj = new JSONObject(json);

        JSONObject machineObj = jsonObj.getJSONObject("catalog");

        catalogJson.setUid(machineObj.optLong("uid"));
        catalogJson.setAccountNamingSchema(machineObj.getString("accountNamingSchema"));
        catalogJson.setAccountNamingSchemaType(machineObj.getEnum(NamingSchemaType.class, "accountNamingSchemaType"));
        catalogJson.setDescription(machineObj.getString("description"));
        catalogJson.setMemoryInMb(machineObj.getInt("memoryInMb"));
        catalogJson.setName(machineObj.getString("name"));
        catalogJson.setNumbersOfProcessors(machineObj.getInt("numbersOfProcessors"));
        catalogJson.setNumbersOfVms(machineObj.getInt("numbersOfVms"));

        return catalogJson;
    }

    /**
     * Extracts the data from the incoming json to generate a delivery group.
     * @param json {@link DeliveryGroupDTO} which holds all the data about the delivery group to create.
     * @return Returns a delivery group json generated out of the given string as a {@link DeliveryGroupDTO} object.
     */
    public static DeliveryGroupDTO convertRawValueToDeliveryGroupJson(String json)
    {
        DeliveryGroupDTO converted = new DeliveryGroupDTO();
        JSONObject jsonObject = new JSONObject(json);

        JSONObject deliveryGroupObj = jsonObject.getJSONObject("deliveryGroup");

        String dgName = deliveryGroupObj.getString("name");
        converted.setName(dgName);

        String dgDescription = deliveryGroupObj.getString("description");
        converted.setDescription(dgDescription);

        // Catalog
        JSONObject catalogObj = deliveryGroupObj.getJSONObject("assignedMachineCatalog");
        MachineCatalogDTO catalogJson = new MachineCatalogDTO();

        catalogJson.setUid(catalogObj.getLong("uid"));

        converted.setAssignedMachineCatalog(catalogJson);

        // Turn on machines after creation
        Boolean dgTurnOnMachinesAfterCreation = deliveryGroupObj.getBoolean("turnOnMachinesAfterCreation");
        converted.setTurnOnMachinesAfterCreation(dgTurnOnMachinesAfterCreation);

        // Obtain all data about the machines to be assigned
        JSONArray machinesArr = deliveryGroupObj.getJSONArray("assignedMachines");
        List<MachineDTO> machineDTOS = new ArrayList<>();
        for (Object obj : machinesArr)
        {
            MachineDTO machineDTO = new MachineDTO();
            JSONObject machineJsonObj = (JSONObject) obj;

            String machineName = machineJsonObj.getString("name");
            machineDTO.setName(machineName);

            int powerState = machineJsonObj.getInt("powerState");
            machineDTO.setPowerState(powerState);

            // Assigned users
            JSONArray usersArr = machineJsonObj.getJSONArray("assignedUsers");
            List<ActiveDirectoryUserDTO> userJsons = new ArrayList<>();
            for (Object userObj : usersArr)
            {
                ActiveDirectoryUserDTO userJson = new ActiveDirectoryUserDTO();
                JSONObject userJsonObj = (JSONObject) userObj;

                String userName = userJsonObj.getString("name");
                userJson.setName(userName);

                String userPrincipalName = userJsonObj.getString("userPrincipalName");
                userJson.setUserPrincipalName(userPrincipalName);

                String userObjectGuid = userJsonObj.getString("objectGuid");
                userJson.setObjectGuid(userObjectGuid);

                String userSid = userJsonObj.getString("sid");
                userJson.setSid(userSid);

                userJsons.add(userJson);
            }
            machineDTO.setAssignedUsers(userJsons);

            // Power actions
            JSONArray actionArr = machineJsonObj.getJSONArray("powerActions");
            List<String> powerActions = new ArrayList<>();
            for (Object powerObj : actionArr)
            {
                String action = String.valueOf(powerObj);
                powerActions.add(action);
            }
            machineDTO.setPowerActions(powerActions);

            machineDTOS.add(machineDTO);
        }
        converted.setAssignedMachines(machineDTOS);

        return converted;
    }

    /**
     * Reads the PowerShell result and converts it to more readable object.
     * @param bufferResult The string that contains the whole JSON which have to be converted.
     * @return Returns a list of {@link ActiveDirectoryUser}s converted from the JSON.
     * @throws JSONException will be thrown if something went wrong while parsing the JSON.
     */
    public static List<ActiveDirectoryUser> convertPowerShellActiveDirectoryUsersResult(String bufferResult) throws JSONException
    {
        List<ActiveDirectoryUser> activeDirectoryUsers = new ArrayList<>();
        JSONArray jsonArray = new JSONArray(bufferResult);

        // Iterate over each object in JSON
        for (Object obj : jsonArray)
        {
            // Cast to from Object to JSONObject
            JSONObject userObject = (JSONObject) obj;

            // Obtain users data from json
            String userName = userObject.getString(POWERSHELL_AD_USER_JSON_VALUE_NAME);
            String userPrincipalName = userObject.getString(POWERSHELL_AD_USER_JSON_VALUE_USER_PRINCIPAL_NAME);
            String userObjectGuid = userObject.getString(POWERSHELL_AD_USER_JSON_VALUE_OBJECT_GUID);
            String userSid = String.valueOf(((JSONObject) userObject.get(POWERSHELL_AD_USER_JSON_VALUE_SID))
                    .get(POWERSHELL_AD_USER_JSON_VALUE_SID_VALUE));

            // Create a new ActiveDirectory user
            ActiveDirectoryUser user = new ActiveDirectoryUser();
            user.setName(userName);
            user.setUserPrincipalName(userPrincipalName);
            user.setObjectGuid(userObjectGuid);
            user.setSid(userSid);

            // Add user to list
            activeDirectoryUsers.add(user);
        }

        return activeDirectoryUsers;
    }

    /**
     * Reads the result coming from the frontend and converts it to more readable object.
     * @param bufferResult The string that contains the whole JSON which have to be converted.
     * @return Returns a list of {@link ActiveDirectoryUser}s converted from the JSON.
     * @throws JSONException will be thrown if something went wrong while parsing the JSON.
     */
    public static List<ActiveDirectoryUser> convertActiveDirectoryUsersResult(String bufferResult) throws JSONException
    {
        List<ActiveDirectoryUser> activeDirectoryUsers = new ArrayList<>();
        JSONArray jsonArray = new JSONArray(bufferResult);

        // Iterate over each object in JSON
        for (Object obj : jsonArray)
        {
            // Cast to from Object to JSONObject
            JSONObject userObject = (JSONObject) obj;

            // Obtain users data from json
            String userName = userObject.getString(AD_USER_JSON_VALUE_NAME);
            String userPrincipalName = userObject.getString(AD_USER_JSON_VALUE_USER_PRINCIPAL_NAME);
            String userObjectGuid = userObject.getString(AD_USER_JSON_VALUE_OBJECT_GUID);
            String userSid = userObject.getString(AD_USER_JSON_VALUE_SID);

            // Create a new ActiveDirectory user
            ActiveDirectoryUser user = new ActiveDirectoryUser();
            user.setName(userName);
            user.setUserPrincipalName(userPrincipalName);
            user.setObjectGuid(userObjectGuid);
            user.setSid(userSid);

            // Add user to list
            activeDirectoryUsers.add(user);
        }

        return activeDirectoryUsers;
    }

    /**
     * Reads the PowerShell result and converts it to more readable object.
     * @param bufferResult The string that contains the whole JSON which have to be converted.
     * @return Returns a list of {@link ActiveDirectoryUser}s converted from the JSON.
     * @throws JSONException will be thrown if something went wrong while parsing the JSON.
     */
    public static List<Machine> convertPowerShellMachinesResultToJson(String bufferResult) throws JSONException
    {
        List<Machine> machines = new ArrayList<>();

        if (bufferResult.startsWith("{"))
        {
            machines.add(readPowerShellMachineJson(new JSONObject(bufferResult)));
        }
        else
        {
            JSONArray jsonArray = new JSONArray(bufferResult);

            // Iterate over each object in JSON
            for (Object obj : jsonArray)
            {
                machines.add(readPowerShellMachineJson((JSONObject) obj));
            }
        }

        return machines;
    }

    /**
     * Reads the string which contains one or multiple machines and converts them into a list of {@link Machine}s.
     * @param jsonObject Object which contains all the data about the machine(s).
     * @return Returns a list of converted {@link Machine}s.
     */
    protected static Machine readPowerShellMachineJson(JSONObject jsonObject)
    {
        // Obtain users data from json
        // UID
        int uid = jsonObject.getInt(POWERSHELL_MC_MACHINE_JSON_VALUE_UID);

        // Hosted machine name
        String name = jsonObject.getString(POWERSHELL_MC_MACHINE_JSON_VALUE_NAME);

        // Delivery group name
        String deliveryGroupName = jsonObject.optString(POWERSHELL_MC_MACHINE_JSON_VALUE_DELIVERY_GROUP_NAME);

        // Catalog name
        String catalogName = jsonObject.optString(POWERSHELL_MC_MACHINE_JSON_VALUE_MACHINE_CATALOG_NAME);
        int catalogUid = jsonObject.optInt(POWERSHELL_MC_MACHINE_JSON_VALUE_MACHINE_CATALOG_UID);

        // Assigned users
        List<ActiveDirectoryUser> assignedUsers = new ArrayList<>();
        JSONArray usersArray = jsonObject.optJSONArray(POWERSHELL_MC_MACHINE_JSON_VALUE_ASSIGNED_USERS);

        for (int i = 0; i < usersArray.length(); i++)
        {
            ActiveDirectoryUser user = new ActiveDirectoryUser();
            user.setUserPrincipalName(usersArray.getString(i));
            assignedUsers.add(user);
        }

        // Power state
        int powerState = jsonObject.getInt(POWERSHELL_MC_MACHINE_JSON_VALUE_POWER_STATE);

        // Supported power actions
        List<String> powerActions = new ArrayList<>();
        JSONArray powerActionsJson = jsonObject.optJSONArray(POWERSHELL_MC_MACHINE_JSON_VALUE_POWER_ACTIONS);

        if (powerActionsJson != null)
        {
            for (int i = 0; i < powerActionsJson.length(); i++)
            {
                powerActions.add(powerActionsJson.getString(i));
            }
        }

        // Create a new ActiveDirectory user
        Machine machine = new Machine();
        machine.setUid(uid);
        machine.setName(name);
        machine.setDeliveryGroupName(deliveryGroupName);
        machine.setCatalogName(catalogName);
        machine.setCatalogUid(catalogUid);
        machine.setAssignedUsers(assignedUsers);
        machine.setPowerActions(powerActions);
        machine.setPowerState(powerState);

        // Add user to list
        return machine;
    }

    /**
     * Reads the result coming from the frontend and converts it to more readable object.
     * @param bufferResult The string that contains the whole JSON which have to be converted.
     * @return Returns a list of {@link ActiveDirectoryUser}s converted from the JSON.
     * @throws JSONException will be thrown if something went wrong while parsing the JSON.
     */
    public static List<Machine> convertRawJsonValueToMachinesList(String bufferResult) throws JSONException
    {
        List<Machine> machines = new ArrayList<>();

        JSONObject json = new JSONObject(bufferResult);
        JSONArray machineArray = json.getJSONArray(MC_MACHINE_JSON_VALUE_MACHINES);

        // Iterate over each object in JSON
        for (Object obj : machineArray)
        {
            machines.add(readMachineJson((JSONObject) obj));
        }

        return machines;
    }

    /**
     * Reads the string which contains one or multiple machines and converts them into a list of {@link Machine}s.
     * @param jsonObject Object which contains all the data about the machine(s).
     * @return Returns a list of converted {@link Machine}s.
     */
    protected static Machine readMachineJson(JSONObject jsonObject)
    {
        // Obtain users data from json
        // Hosted machine name
        int uid = jsonObject.getInt(MC_MACHINE_JSON_VALUE_UID);

        // Hosted machine name
        String name = jsonObject.getString(MC_MACHINE_JSON_VALUE_NAME);

        // Delivery group name
        String deliveryGroupName = jsonObject.optString(MC_MACHINE_JSON_VALUE_DELIVERY_GROUP_NAME);

        // Catalog name
        String catalogName = jsonObject.optString(MC_MACHINE_JSON_VALUE_CATALOG_NAME);
        int catalogUid = jsonObject.optInt(MC_MACHINE_JSON_VALUE_CATALOG_UID);

        // Assigned users
        List<ActiveDirectoryUser> assignedUsers = new ArrayList<>();
        JSONArray usersArray = jsonObject.optJSONArray(MC_MACHINE_JSON_VALUE_ASSIGNED_USERS);

        for (int i = 0; i < usersArray.length(); i++)
        {
            JSONObject userObj = usersArray.getJSONObject(i);

            ActiveDirectoryUser user = new ActiveDirectoryUser();
            user.setUserPrincipalName(userObj.getString(AD_USER_JSON_VALUE_USER_PRINCIPAL_NAME));
            user.setSid(userObj.optString(AD_USER_JSON_VALUE_SID));
            user.setObjectGuid(userObj.optString(AD_USER_JSON_VALUE_OBJECT_GUID));
            user.setName(userObj.optString(AD_USER_JSON_VALUE_NAME));

            assignedUsers.add(user);
        }

        // Power state
        int powerState = jsonObject.getInt(MC_MACHINE_JSON_VALUE_POWER_STATE);

        // Supported power actions
        List<String> powerActions = new ArrayList<>();
        JSONArray powerActionsJson = jsonObject.optJSONArray(MC_MACHINE_JSON_VALUE_POWER_ACTIONS);

        if (powerActionsJson != null)
        {
            for (int i = 0; i < powerActionsJson.length(); i++)
            {
                powerActions.add(powerActionsJson.getString(i));
            }
        }

        // Create a new ActiveDirectory user
        Machine machine = new Machine();
        machine.setUid(uid);
        machine.setName(name);
        machine.setDeliveryGroupName(deliveryGroupName);
        machine.setCatalogName(catalogName);
        machine.setCatalogUid(catalogUid);
        machine.setAssignedUsers(assignedUsers);
        machine.setPowerActions(powerActions);
        machine.setPowerState(powerState);

        // Add user to list
        return machine;
    }

    /**
     * Reads the raw JSON value and extract the result to build a {@link PowerShellResponse}.
     * @param rawJsonResult Contains the result of the power action execution.
     * @return Returns a {@link PowerShellResponse} containing the result of the power action execution.
     */
    public static PowerShellResponse convertPowerActionExecutionResult(String rawJsonResult, String machineName)
    {
        // Should always be one object
        JSONObject jsonObject = new JSONObject(rawJsonResult);

        Integer processUid = jsonObject.getInt("Uid");
        String machineNameOfResult = jsonObject.getString("MachineName").split("\\\\")[1];

        PowerShellResponse response = new PowerShellResponse();
        if (StringUtils.equals(machineNameOfResult, machineName))
        {
            response.setResult(0);
            response.setStatus(PowerShellStatus.SUCCESS);
            response.setMessage("Power action successfully performed on: " + machineName);
        }

        return response;
    }

}
