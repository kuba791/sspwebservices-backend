package com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.user_rs;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public class UserPropertiesJson
{
    public Long userId;
    public Map<String, String> properties;

    @JsonCreator
    public UserPropertiesJson(@JsonProperty("user_id") Long userId, @JsonProperty("properties") Map<String, String> properties)
    {
        this.userId = userId;
        this.properties = properties;
    }
}
