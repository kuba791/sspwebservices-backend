package com.fhswf.kandziora.kevin.sspwebservicesbackend.service.citrix;

import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.deliverygroup.DeliveryGroup;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.persistence.webservice.citrix.DeliveryGroupRepository;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;

@Service
public class DeliverGroupService
{
    private static final Logger LOGGER = Logger.getLogger(DeliverGroupService.class);

    protected DeliveryGroupRepository repository;

    /**
     * Saves the given {@link DeliveryGroup}.
     * @param deliveryGroup {@link DeliveryGroup}.
     * @return Returns the persisted {@link DeliveryGroup} when saving was successful, else {@code null}.
     */
    public DeliveryGroup saveDeliveryGroup(DeliveryGroup deliveryGroup)
    {
        try
        {
            return repository.save(deliveryGroup);
        }
        catch (Exception e)
        {
            LOGGER.error("Failed to save/update delivery group: " + e.getMessage());
        }
        return null;
    }

    /**
     * Get a list of all {@link DeliveryGroup} of the given user.
     * @param userId ID of the user.
     * @return Returns a list of the user's {@link DeliveryGroup}s.
     */
    public List<DeliveryGroup> getAllDeliveryGroupsByUserId(Long userId)
    {
        List<DeliveryGroup> deliveryGroups = repository.findDeliveryGroupsByUserId(userId);

        if (CollectionUtils.isEmpty(deliveryGroups))
        {
            return Collections.emptyList();
        }
        return deliveryGroups;
    }

    /**
     * Get a list of UIDs of all {@link DeliveryGroup}s of the given user.
     * @param userId ID of the user.
     * @return Returns a list of UIDs of all {@link DeliveryGroup}s of the given user.
     */
    public List<String> getAllDeliveryGroupUidsByUserId(Long userId)
    {
        List<String> uids = repository.findAllDeliveryGroupUidsByUserId(userId);

        if (CollectionUtils.isEmpty(uids))
        {
            return Collections.emptyList();
        }
        return uids;
    }

    // ----- GETTER & SETTER -----
    public DeliveryGroupRepository getRepository()
    {
        return repository;
    }

    @Autowired
    public void setRepository(DeliveryGroupRepository repository)
    {
        this.repository = repository;
    }
}
