package com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user;

import java.io.Serializable;

//@JsonFormat(shape=JsonFormat.Shape.OBJECT)
public class ActiveDirectoryUser implements Serializable
{
    private static final long serialVersionUID = -3924310255827994648L;

    private String name;
    private String userPrincipalName;
    private String objectGuid;
    private String sid;

    /**
     * Empty constructor
     */
    public ActiveDirectoryUser()
    {
        // intentionally empty
    }

    // ----- GETTER & SETTER -----
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getUserPrincipalName()
    {
        return userPrincipalName;
    }

    public void setUserPrincipalName(String userPrincipalName)
    {
        this.userPrincipalName = userPrincipalName;
    }

    public String getObjectGuid()
    {
        return objectGuid;
    }

    public void setObjectGuid(String objectGuid)
    {
        this.objectGuid = objectGuid;
    }

    public String getSid()
    {
        return sid;
    }

    public void setSid(String sid)
    {
        this.sid = sid;
    }
}
