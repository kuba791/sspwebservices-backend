package com.fhswf.kandziora.kevin.sspwebservicesbackend.utilities.powershell;

import java.util.Arrays;
import java.util.Optional;

public enum PowerShellStatus
{
    SUCCESS(0, "Process was successful"),
    UNKNOWN(-1, "Error is not known."),
    MACHINE_CATALOG_PROVISIONING_TASK_FAILED_GENERAL(-2, "A general error occurred in the provisioning process"),
    MACHINE_CATALOG_IDENTITY_POOL_NAME_ALREADY_EXISTING(-3, "Pool name exists already."),
    MACHINE_CATALOG_PROVISIONING_SCHEME_ALREADY_EXISTING(-4, "Naming scheme exists already."),
    DELIVER_GROUP_ALREADY_EXISTS(-5, "Delivery group already exists."),
    MACHINE_CATALOG_ALREADY_EXISTS(-6, "Machine catalog already exists."),
    TOO_MANY_DELIVERY_GROUPS_WHILE_ADDING(-7, "Too many delivery groups found, while adding machines."),
    POWER_SHELL_INCORRECT_USE_FAILURE(-100, "PowerShell process was incorrect used.");

    private final int statusCode;
    private final String description;

    /**
     * Constructor
     * @param statusCode Status code as negative integer.
     * @param description Description to the error code.
     */
    PowerShellStatus(int statusCode, String description)
    {
        this.statusCode = statusCode;
        this.description = description;
    }

    /**
     * Finds the {@link PowerShellStatus} based on the given error code.
     * @param statusCode Status code that was delivered by the result of the executed powershell script.
     * @return Returns a {@link PowerShellStatus} based on the given status code.
     */
    public static PowerShellResponse getResponseByStatusCode(int statusCode)
    {
        PowerShellResponse response = new PowerShellResponse();

        if (statusCode >= 0)
        {
            response.setResult(statusCode);
            response.setStatus(PowerShellStatus.SUCCESS);
        }
        else
        {
            Optional<PowerShellStatus> statusOptional = Arrays.stream(PowerShellStatus.values())
                    .filter(status -> status.getStatusCode() == statusCode)
                    .findFirst();

            statusOptional.ifPresent(response::setStatus);
        }
        return response;
    }

    // ----- GETTER -----
    public int getStatusCode()
    {
        return statusCode;
    }

    public String getDescription()
    {
        return description;
    }
}
