package com.fhswf.kandziora.kevin.sspwebservicesbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.WebApplicationInitializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.util.Properties;

@SpringBootApplication
public class SspWebServicesBackendApplication extends SpringBootServletInitializer
{
	public static void main(String[] args)
	{
		Properties properties = System.getProperties();
		SpringApplication.run(SspWebServicesBackendApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder applicationBuilder)
	{
		return applicationBuilder.sources(SspWebServicesBackendApplication.class);
	}
}
