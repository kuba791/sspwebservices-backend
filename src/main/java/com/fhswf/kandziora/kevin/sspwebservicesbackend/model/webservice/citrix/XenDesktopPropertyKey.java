package com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix;

public class XenDesktopPropertyKey
{
    public static final String ACCOUNT_NAMING_SCHEMA      = "accountNamingSchema";
    public static final String ACCOUNT_NAMING_SCHEMA_TYPE = "accountNamingSchemaType";
    public static final String CATALOG_DESCRIPTION        = "machineCatalogDescription";
    public static final String CATALOG_NAME               = "machineCatalogName";
    public static final String MACHINE_MEMORY_MB          = "memoryInMb";
    public static final String MACHINE_NUMBERS_OF_CPUS    = "numbersOfProcessors";
    public static final String NUMBERS_OF_VMS             = "numbersOfVms";
    public static final String AD_ORGANIZATION_UNIT       = "organizationUnit";

    public static final String DELIVERY_GROUP_NAME         = "name";
    public static final String DELIVERY_GROUP_DESCRIPTION  = "description";
    public static final String DELIVERY_GROUP_USER_SIDS    = "userSids";
    public static final String DELIVERY_GROUP_CATALOG_NAME = "catalogName";
}
