package com.fhswf.kandziora.kevin.sspwebservicesbackend.utilities.powershell;

import java.io.Serializable;

public class PowerShellResponse implements Serializable
{
    private static final long serialVersionUID = -4042788124816998895L;

    private Integer result;
    private PowerShellStatus status;
    private String message;

    public PowerShellResponse()
    {
        this.result = null;
        this.status = PowerShellStatus.UNKNOWN;
        this.message = "";
    }

    public PowerShellResponse(Integer result, PowerShellStatus status, String message)
    {
        this.result = result;
        this.status = status;
        this.message = message;
    }

    // ----- GETTER & SETTER -----

    public Integer getResult()
    {
        return result;
    }

    public void setResult(Integer result)
    {
        this.result = result;
    }

    public PowerShellStatus getStatus()
    {
        return status;
    }

    public void setStatus(PowerShellStatus status)
    {
        this.status = status;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
}
