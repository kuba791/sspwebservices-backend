package com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user;

public enum UserRole
{
    USER("USER"),
    ADMIN("ADMIN");

    private String role;

    private UserRole(String role)
    {
        this.role = role;
    }

    // ----- GETTERS & SETTERS -----

    public String getRole()
    {
        return role;
    }

    public void setRole(String role)
    {
        this.role = role;
    }
}
