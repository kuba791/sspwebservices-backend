package com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.machinecatalog;

import com.fasterxml.jackson.annotation.JsonValue;

public enum NamingSchemaType
{
    NUMERIC("numeric"),
    ALPHABETIC("alphabetic");

    private String type;

    NamingSchemaType(String type)
    {
        this.type = type;
    }

    @JsonValue
    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }
}