package com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.webservice_rs.citrix.deliverygroup;

import com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.webservice_rs.citrix.machine.MachineDTO;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.webservice_rs.citrix.machinecatalog.MachineCatalogDTO;

import java.util.List;

public class DeliveryGroupDTO
{
    // Delivery group details
    public String name;
    public String description;
    public boolean turnOnMachinesAfterCreation;
    public MachineCatalogDTO assignedMachineCatalog;
    public List<MachineDTO> assignedMachines;

    /**
     * Default constructor
     */
    public DeliveryGroupDTO()
    {
        // intentionally empty
    }

    public DeliveryGroupDTO(String name, String description, boolean turnOnMachinesAfterCreation,
                            MachineCatalogDTO assignedMachineCatalog, List<MachineDTO> assignedMachines)
    {
        this.name = name;
        this.description = description;
        this.turnOnMachinesAfterCreation = turnOnMachinesAfterCreation;
        this.assignedMachineCatalog = assignedMachineCatalog;
        this.assignedMachines = assignedMachines;
    }

    // ----- GETTER & SETTER -----
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public boolean isTurnOnMachinesAfterCreation()
    {
        return turnOnMachinesAfterCreation;
    }

    public void setTurnOnMachinesAfterCreation(boolean turnOnMachinesAfterCreation)
    {
        this.turnOnMachinesAfterCreation = turnOnMachinesAfterCreation;
    }

    public MachineCatalogDTO getAssignedMachineCatalog()
    {
        return assignedMachineCatalog;
    }

    public void setAssignedMachineCatalog(MachineCatalogDTO assignedMachineCatalog)
    {
        this.assignedMachineCatalog = assignedMachineCatalog;
    }

    public List<MachineDTO> getAssignedMachines()
    {
        return assignedMachines;
    }

    public void setAssignedMachines(List<MachineDTO> assignedMachines)
    {
        this.assignedMachines = assignedMachines;
    }
}
