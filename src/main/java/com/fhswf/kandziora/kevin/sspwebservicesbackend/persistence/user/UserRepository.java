package com.fhswf.kandziora.kevin.sspwebservicesbackend.persistence.user;

import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user.User;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user.UserProperty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long>
{
    Optional<User> findByMail(String mail);
    Optional<User> findById(Long id);

    @Query("SELECT up FROM UserProperty up WHERE up.user.id = ?1")
    List<UserProperty> findUserPropertiesByUserId(Long id);
}
