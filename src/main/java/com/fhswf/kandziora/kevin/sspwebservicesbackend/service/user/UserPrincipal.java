package com.fhswf.kandziora.kevin.sspwebservicesbackend.service.user;

import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user.User;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user.UserRole;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

/**
 * Encapsulates the incoming {@link User} from the login for security reasons.
 */
public class UserPrincipal implements UserDetails
{
    private static final long serialVersionUID = 2831516611862397742L;

    protected final User user;

    public UserPrincipal(User user)
    {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities()
    {
        return Collections.<GrantedAuthority>
                singletonList(new SimpleGrantedAuthority(String.valueOf(UserRole.USER)));
    }

    @Override
    public String getPassword()
    {
        return user.getPassword();
    }

    @Override
    public String getUsername()
    {
        return user.getMail();
    }

    @Override
    public boolean isAccountNonExpired()
    {
        return true;
    }

    @Override
    public boolean isAccountNonLocked()
    {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired()
    {
        return true;
    }

    @Override
    public boolean isEnabled()
    {
        return user.isActive();
    }

    public Long getId()
    {
        return user.getId();
    }
}
