package com.fhswf.kandziora.kevin.sspwebservicesbackend.service.citrix;

import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.machinecatalog.MachineCatalog;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.persistence.webservice.citrix.MachineCatalogRepository;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;

@Service
public class MachineCatalogService
{
    private static final Logger LOGGER = Logger.getLogger(MachineCatalogService.class);

    protected MachineCatalogRepository repository;

    /**
     * Saves the given {@link MachineCatalog}.
     * @param catalog {@link MachineCatalog}.
     * @return Returns the persisted {@link MachineCatalog} when saving was successful, else {@code null}.
     */
    public MachineCatalog createAndSaveMachineCatalog(MachineCatalog catalog)
    {
        try
        {
            return repository.save(catalog);
        }
        catch (Exception e)
        {
            LOGGER.error("Failed to save/update machine catalog: " + e.getMessage());
        }
        return null;
    }

    /**
     * Get a list of all {@link MachineCatalog} of the given user.
     * @param userId ID of the user
     * @return Returns a list of the user's {@link MachineCatalog}s.
     */
    public List<MachineCatalog> getAllMachineCatalogsByUserId(Long userId)
    {
        List<MachineCatalog> machineCatalogs = repository.findMachineCatalogsByUserId(userId);

        if (CollectionUtils.isEmpty(machineCatalogs))
        {
            return Collections.emptyList();
        }
        return machineCatalogs;
    }

    /**
     * Get a list of UIDs of all {@link MachineCatalog}s of the given user.
     * @param userId ID of the user.
     * @return Returns a list of UIDs of all {@link MachineCatalog}s of the given user.
     */
    public List<Integer> getAllMachineCatalogUidsByUserId(Long userId)
    {
        List<Integer> uids = repository.findAllMachineCatalogUidsByUserId(userId);

        if (CollectionUtils.isEmpty(uids))
        {
            return Collections.emptyList();
        }
        return uids;
    }

    // ----- GETTER & SETTER -----
    public MachineCatalogRepository getRepository()
    {
        return repository;
    }

    @Autowired
    public void setRepository(MachineCatalogRepository repository)
    {
        this.repository = repository;
    }
}
