package com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.webservice_rs.citrix.deliverygroup;

import com.fhswf.kandziora.kevin.sspwebservicesbackend.api.v1.webservice_rs.citrix.machinecatalog.MachineCatalogServiceController;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user.User;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.deliverygroup.DeliveryGroup;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.model.webservice.citrix.machine.Machine;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.service.citrix.DeliverGroupService;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.service.user.UserService;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.utilities.JsonUtilities;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.utilities.powershell.PowerShellCommander;
import com.fhswf.kandziora.kevin.sspwebservicesbackend.utilities.powershell.PowerShellResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin()
@RequestMapping("/users/{userId}/web-services/delivery-groups")
public class DeliveryGroupServiceController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(MachineCatalogServiceController.class);

    protected DeliverGroupService deliverGroupService;
    protected UserService userService;

    /**
     * Constructor
     * @param deliverGroupService Service to handle all necessary delivery group operations
     * @param userService Service to handle all necessary user operations
     */
    public DeliveryGroupServiceController(DeliverGroupService deliverGroupService, UserService userService)
    {
        this.deliverGroupService = deliverGroupService;
        this.userService = userService;
    }

    /**
     * Handles the GET request of listing all delivery groups of the given user.
     * @param userId User ID for identifying all delivery groups of this user.
     * @return Returns a response with all delivery groups of the user JSON formatted.
     */
    @GetMapping()
    public ResponseEntity<?> getUsersDeliveryGroups(@PathVariable Long userId)
    {
        LOGGER.info("getUsersDeliveryGroups()");

        List<DeliveryGroup> deliveryGroups = deliverGroupService.getAllDeliveryGroupsByUserId(userId);

        return new ResponseEntity<>(deliveryGroups, HttpStatus.OK);
    }

    /**
     * Handles the POST request of creating a delivery group.
     * @param userId UserId which should be assigned to the delivery group to be created.
     * @param rawDeliveryGroupJson A raw JSON string which holds the data coming from the frontend.
     * @return Returns a response depending on the result of the delivery group creation process.
     */
    @PostMapping()
    public ResponseEntity<?> createDeliveryGroup(@PathVariable Long userId,
                                                 @RequestBody String rawDeliveryGroupJson)
    {
        // Extract data from raw JSON
        DeliveryGroupDTO deliveryGroupDTO = JsonUtilities.convertRawValueToDeliveryGroupJson(rawDeliveryGroupJson);

        // Generate delivery group object out of incoming json
        DeliveryGroup deliveryGroup = generateDeliveryGroup(userId, deliveryGroupDTO);

        // Execute powershell script to create a new delivery group
        PowerShellResponse response = PowerShellCommander.createNewDeliveryGroup(deliveryGroup);

        // If creating the delivery group was successful, save it in the db
        if (response.getResult() != null && response.getResult() > 0)
        {
            LOGGER.info("Delivery group was succesfully created");

            // Set UID of the delivery group
            deliveryGroup.setUid(Long.valueOf(response.getResult()));

            // Save delivery group information in the database
            DeliveryGroup createdDeliveryGroup = deliverGroupService.saveDeliveryGroup(deliveryGroup);

            // Check if saving was successful
            if (createdDeliveryGroup == null)
            {
                LOGGER.error("Failed to save delivery group");
                return new ResponseEntity<>("Failed to persist delivery group after creation.", HttpStatus.BAD_REQUEST);
            }
            LOGGER.info("Successfully saved delivery group with ID: " + createdDeliveryGroup.getUid());
            return new ResponseEntity<>(createdDeliveryGroup, HttpStatus.OK);
        }
        else
        {
            // Return error response to frontend
            LOGGER.error("Creating delivery group went wrong. PowerShellCommander returned: "
                    + response.getStatus().getStatusCode());
            return new ResponseEntity<>(response.getStatus().getDescription(), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Put resource to add machines to the selected delivery group.
     * @param userId UserId to identify the selected delivery group.
     * @param rawMachinesValue A raw JSON string which holds the data coming from the frontend.
     * @return Returns a response holding the result of the process.
     */
    @PutMapping("/add-machines")
    public ResponseEntity<?> addMachinesToDeliveryGroup(@PathVariable Long userId, @RequestBody String rawMachinesValue)
    {
        // Converts the incoming json string to a list of machines
        List<Machine> machines = JsonUtilities.convertRawJsonValueToMachinesList(rawMachinesValue);

        // Execute PowerShell process
        // TODO: PROD
        PowerShellResponse response = PowerShellCommander.addMachinesToDeliveryGroup(machines);
        // TODO: DEV
//        PowerShellResponse response = new PowerShellResponse();
//        response.setResult(0);
//        response.setStatus(PowerShellStatus.SUCCESS);

        // Return response
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    // ----- HELPER METHODS -----
    /**
     * Extracts the data from the incoming json to generate a delivery group.
     * @param userId ID of the user who creates this delivery group and will be assigned to.
     * @param deliveryGroupDTO {@link DeliveryGroupDTO} which holds all the data about the delivery group to create.
     * @return Returns a delivery group generated out of the given JSON as a {@link DeliveryGroup} object.
     */
    protected DeliveryGroup generateDeliveryGroup(Long userId, DeliveryGroupDTO deliveryGroupDTO)
    {
        // Load user
        User user = userService.getUserById(userId);

        DeliveryGroup deliveryGroup = new DeliveryGroup();
        deliveryGroup.setUser(user);
        deliveryGroup.setName(deliveryGroupDTO.getName());
        deliveryGroup.setDescription(deliveryGroupDTO.getDescription());
        deliveryGroup.setCatalogUid(deliveryGroupDTO.assignedMachineCatalog.getUid());
        deliveryGroup.setMachines(deliveryGroupDTO.getAssignedMachines());
        deliveryGroup.setTurnOnAddedMachines(deliveryGroupDTO.isTurnOnMachinesAfterCreation());

        return deliveryGroup;
    }
}
