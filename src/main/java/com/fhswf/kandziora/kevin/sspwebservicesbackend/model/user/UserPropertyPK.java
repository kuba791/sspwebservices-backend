package com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * Class only used for the composite key
 */
@Embeddable
public class UserPropertyPK implements Serializable
{
    private static final long serialVersionUID = 5913453662649685534L;

    @Column(name="property_key")
    @NotNull
    private String propertyKey;

    private Long userId;

    // ----- GETTER & SETTER -----

    public String getPropertyKey()
    {
        return propertyKey;
    }

    public void setPropertyKey(String propertyKey)
    {
        this.propertyKey = propertyKey;
    }

    public Long getUserId()
    {
        return userId;
    }

    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    // ----- EQUALS & HASHCODE -----

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPropertyPK that = (UserPropertyPK) o;
        return Objects.equals(propertyKey, that.propertyKey) && Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(propertyKey, userId);
    }
}