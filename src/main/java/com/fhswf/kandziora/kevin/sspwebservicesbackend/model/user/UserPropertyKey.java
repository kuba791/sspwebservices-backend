package com.fhswf.kandziora.kevin.sspwebservicesbackend.model.user;

/**
 * Class holding property keys for the {@link UserProperty} class.
 */
public class UserPropertyKey
{
    // LDAP connection constants
    public static final String AD_DOMAIN             = "activedirectory.domain";
    public static final String AD_ORG_UNIT_COMPUTERS = "activedirectory.organizationunit.computers";
    public static final String AD_ORG_UNIT_USERS     = "activedirectory.organizationunit.users";

    // Constants for XenDesktop
    public static final String XENDESKTOP_XD_CONTROLLER_ADMIN_ADDRESS = "xendesktop.xdcontroller.adminaddress";
    public static final String XENDESKTOP_XD_CONTROLLER_CONTROLLERS = "xendesktop.xdcontroller.controllers";
    public static final String XENDESKTOP_STORAGE_RESOURCE = "xendesktop.storage.resource";
    public static final String XENDESKTOP_HOST_RESOURCE = "xendesktop.host.resource";
    public static final String XENDESKTOP_ALLOCATION_TYPE = "xendesktop.allocation.type";
    public static final String XENDESKTOP_PERSIST_CHANGES = "xendesktop.persist.changes";
    public static final String XENDESKTOP_PROVISIONING_TYPE = "xendesktop.provisioning.type";
    public static final String XENDESKTOP_SESSION_SUPPORT = "xendesktop.session.support";
    public static final String XENDESKTOP_MASTER_IMAGE_PATH = "xendesktop.masterimage.path";
}
