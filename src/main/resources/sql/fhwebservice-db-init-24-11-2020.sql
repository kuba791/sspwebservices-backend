CREATE DATABASE IF NOT exists `fhwebservice`;

-- CREATE USER TABLE
CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `mail` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'0',
  `role` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- CREATE USER_PROPERTY TABLE
CREATE TABLE IF NOT EXISTS `user_property` (
  `property_key` varchar(500) COLLATE utf8mb4_bin NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`property_key`,`user_id`),
  KEY `fk_UserUserProperty` (`user_id`),
  CONSTRAINT `fk_UserUserProperty` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- CREATE MACHINE_CATALOG TABLE
CREATE TABLE IF NOT EXISTS `machine_catalog` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `uid` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`,`user_id`),
  KEY `fk_UserMachineCatalog` (`user_id`),
  CONSTRAINT `fk_UserMachineCatalog` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- CREATE DELIVERY_GROUP TABLE
CREATE TABLE IF NOT EXISTS `delivery_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `uid` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`,`user_id`),
  KEY `fk_UserDeliveryGroup` (`user_id`),
  CONSTRAINT `fk_UserDeliveryGroup` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

