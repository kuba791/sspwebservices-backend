#---------------------------------------------------------------------------
# Author:      Aaron Parker
# Desc:        Using PowerShell to create a XenDesktop Delivery Group
# Date:        Aug 23, 2014
# Site:        http://stealthpuppy.com
# Editor:      Kevin Kandziora
# Update-date: Nov 22, 2020
#---------------------------------------------------------------------------

# Define parameters which will be used for creating a delivery group for several machine catalogs
[CmdletBinding()]
param
(
    # Desktop Group properties
    [Parameter(Mandatory=$true)] [string] $deliveryGroupName,
    [Parameter(Mandatory=$true)] [string] $description,
    [Parameter(Mandatory=$true)] [int] $catalogUID,
    [Parameter(Mandatory=$true)] [hashtable] $userAssignedMachines,
    [Parameter(Mandatory=$true)] [bool] $turnOnAddedMachines
)

# Add Citrix necessary powershell snap-ins
Add-PSSnapin Citrix*

# Set necessary variables
$adminAddress = "xendesktop.win2016s01.fhswf.de";
$turnOnAddedMachines = $true

# Create the Desktop Group
If (!(Get-BrokerDesktopGroup -Name $deliveryGroupName -ErrorAction SilentlyContinue))
{
    Write-Output "Creating new Desktop Group: $deliveryGroupName"
    $desktopGroup = New-BrokerDesktopGroup -ErrorAction SilentlyContinue -AdminAddress $adminAddress -Name $deliveryGroupName -Description $description -PublishedName $deliveryGroupName -DesktopKind Private -MinimumFunctionalLevel 'L7_20' -SessionSupport 'SingleSession' -ShutdownDesktopsAfterUse $True -InMaintenanceMode $False -IsRemotePC $False -SecureIcaRequired $False -TurnOnAddedMachine $turnOnAddedMachines -OffPeakDisconnectAction Suspend -OffPeakDisconnectTimeout 15 -Scope @()
}

# At this point, we have a Desktop Group, but no users or desktops assigned to it, no power management etc.
# Open the properties of the new Desktop Group to see what's missing.
# If creation of the desktop group was successful, continue modifying its properties
If ($desktopGroup)
{
    # Add machines to the new desktop group. Uses the number of machines available in the target machine catalog
    $machineCatalog = Get-BrokerCatalog -AdminAddress $adminAddress -Uid $catalogUID
    $catalogName = $machineCatalog.Name

    Write-Output "Get all users which should be assigned"
    $userSIDs = @()
    foreach ($machine in $userAssignedMachines.Keys)
    {
        foreach ($userSID in $userAssignedMachines[$machine])
        {
            Write-Output "Found user SID: $userSID"
            $userSIDs += $userSID
        }
    }

    # Create a new broker user/group object if it doesn't already exist
    Write-Output "Creating user/group object in the broker"
    $brokerUsers = @()
    foreach ($userSID in $userSIDs)
    {
        If (!(Get-BrokerUser -AdminAddress $adminAddress -SID $userSID -ErrorAction SilentlyContinue))
        {
            $brokerUsers += New-BrokerUser -AdminAddress $adminAddress -SID $userSID
        }
        Else
        {
            $brokerUsers += Get-BrokerUser -AdminAddress $adminAddress -SID $userSID
        }
    }
    Write-Output "Successfully created new broker users: $brokerUsers"

    # Create an entitlement policy for the new desktop group. Assigned users to the desktop group
    # First check that we have an entitlement name available. Increment until we do.
    $Num = 1
    Do
    {
        $Test = Test-BrokerEntitlementPolicyRuleNameAvailable -AdminAddress $adminAddress -Name @($deliveryGroupName + "_" + $Num.ToString()) -ErrorAction SilentlyContinue
        If ($Test.Available -eq $False) { $Num = $Num + 1 }
    }
    While ($Test.Available -eq $False)

    #Write-Output "Assigning $brokerUsers.Name to Desktop Catalog: $catalogName"
    #$EntPolicyRule = New-BrokerEntitlementPolicyRule -AdminAddress $adminAddress -Name ($deliveryGroupName + "_" + $Num.ToString()) -IncludedUsers $brokerUsers -DesktopGroupUid $desktopGroup.Uid -Enabled $True -IncludedUserFilterEnabled $False

    # Check whether access rules exist and then create rules for direct access and via Access Gateway
    $accessPolicyRule = $deliveryGroupName + "_Direct"
    If (Test-BrokerAccessPolicyRuleNameAvailable -AdminAddress $adminAddress -Name @($accessPolicyRule) -ErrorAction SilentlyContinue)
    {
        Write-Output "Allowing direct access rule to the Desktop Catalog: $catalogName"
        New-BrokerAccessPolicyRule -AdminAddress $adminAddress -Name $accessPolicyRule -IncludedUsers @($brokerUsers.Name) -AllowedConnections 'NotViaAG' -AllowedProtocols @('HDX','RDP') -AllowRestart $True -DesktopGroupUid $desktopGroup.Uid -Enabled $True -IncludedSmartAccessFilterEnabled $True -IncludedUserFilterEnabled $True
    }
    Else
    {
        Write-Error "Failed to add direct access rule $accessPolicyRule. It already exists."
    }

    $accessPolicyRule = $deliveryGroupName + "_AG"
    If (Test-BrokerAccessPolicyRuleNameAvailable -AdminAddress $adminAddress -Name @($accessPolicyRule) -ErrorAction SilentlyContinue)
    {
        Write-Output "Allowing access via Access Gateway rule to the Desktop Catalog: $catalogName"
        New-BrokerAccessPolicyRule -AdminAddress $adminAddress -Name $accessPolicyRule -IncludedUsers @($brokerUsers.Name) -AllowedConnections 'ViaAG' -AllowedProtocols @('HDX','RDP') -AllowRestart $True -DesktopGroupUid $desktopGroup.Uid -Enabled $True -IncludedSmartAccessFilterEnabled $True -IncludedSmartAccessTags @() -IncludedUserFilterEnabled $True
    }
    Else
    {
        Write-Error "Failed to add Access Gateway rule $accessPolicyRule. It already exists."
    }

    # Assign user(s) to corresponding machine
    Write-Output "Assign broker users to dekstops in the delivery group"

    foreach ($machineName in $userAssignedMachines.Keys)
    {
        # Get machine by name
        Write-Output "Machine name: $machineName"

        $brokerMachine = Get-BrokerMachine -HostedMachineName $machineName
        $brokerMachineName = $brokerMachine.MachineName

        Write-Output "Retrieved broker machine: $brokerMachineName"

        # Add machine to desktop group (Could be merged with line 121)
        Add-BrokerMachine -MachineName $brokerMachine.MachineName -DesktopGroup $desktopGroup

        # Assign users from variable to current machine
        foreach ($userSID in $userAssignedMachines[$machineName])
        {
            # Get machine by name
            Write-Output "User sid: $userSID"

            # Get broker user from brokerUsers var
            $brokerUser = Get-BrokerUser -SID $userSID
            $brokerUserName = $brokerUser.UPN
            Write-Output "Retrieved broker user: $brokerUserName"

            # Assign found user to current machine
            Add-BrokerUser -Name $brokerUser.Name -PrivateDesktop $brokerMachine.MachineName
        }
    }

    Write-Output $desktopGroup.Uid -InformationAction Stop
}
else
{
    Write-Error "Delivery group already exists"
    Write-Error -5 -ErrorAction Stop
}