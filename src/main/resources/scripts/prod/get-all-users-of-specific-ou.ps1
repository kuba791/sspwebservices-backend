﻿#---------------------------------------------------------------------------
# Author:      Kevin Kandziora
# Desc:        Using PowerShell to get all users of a specific organization unit
# Date:        Nov 19, 2020
#---------------------------------------------------------------------------

[CmdletBinding()]
param
(
   [Parameter(Mandatory=$true)] [string] $OUPath
)

# Add Citrix necessary powershell snap-ins
#Add-PSSnapin ActiveDirectory

#$OUpath = "OU=Users,OU=Doe,OU=SSP,DC=win2016s01,DC=fhswf,DC=de"

$userList = Get-ADUser -Filter 'ObjectClass -eq "user"' -SearchBase $OUpath | Select-Object Name, UserPrincipalName, ObjectGUID, SID | ConvertTo-Json

return $userList