﻿#
# Script to get all machines of a given machine catalog
#

[CmdletBinding()]
param
(
    [Parameter(Mandatory=$true)] [int] $catalogUID,
    [Parameter(Mandatory=$false)] [bool] $allMachines,
    [Parameter(Mandatory=$false)] [bool] $assignedToDesktopGroup
)

# Set necessary parameters
$adminAddress = "xendesktop.win2016s01.fhswf.de";


if (!($allMachines))
{
    # Get all machines which are assigned to a desktop group
    if ($assignedToDesktopGroup)
    {
        $machines = (Get-BrokerMachine -AdminAddress $adminAddress -CatalogUid $catalogUID) | Where-Object {$null -ne $_.DesktopGroupUid} | Select-Object HostedMachineName, Uid, CatalogUid, AssociatedUserUPNs, PowerState, SupportedPowerActions | ConvertTo-Json
    }
    # Get all machines which are not assigned to a desktop group
    else
    {
        $machines = (Get-BrokerMachine -AdminAddress $adminAddress -CatalogUid $catalogUID) | Where-Object {$null -eq $_.DesktopGroupUid} | Select-Object HostedMachineName, Uid, CatalogUid, AssociatedUserUPNs, PowerState, SupportedPowerActions | ConvertTo-Json
    }
}
# Get all machines of catalog
else
{
    $machines = (Get-BrokerMachine -AdminAddress $adminAddress -CatalogUid $catalogUID) | Select-Object HostedMachineName, Uid, CatalogUid, AssociatedUserUPNs, PowerState, SupportedPowerActions | Where-Object {$null -eq $_.DesktopGroupUid} | ConvertTo-Json
}

return $machines