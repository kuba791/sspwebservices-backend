#
# Script to get all machines of a specific broker/machine catalog
#

[CMmdletBinding()]
param
(
    [Parameter(Mandatory=$true)] [int] $catalogUid
)

# Add Citrix necessary powershell snap-ins
Add-PSSnapin Citrix*

# For collecting specific properties use out commented line
# return Get-BrokerMachine -CatalogUid $catalogUid | Select-Object -Property <propertyToSelect>
return Get-BrokerMachine -CatalogUid $catalogUid