#---------------------------------------------------------------------------
# Author:      Aaron Parker
# Desc:        Using PowerShell to create a XenDesktop 7.x Delivery Group
# Date:        Aug 23, 2014
# Site:        http://stealthpuppy.com
# Editor:      Kevin Kandziora
# Update-date: Nov 15, 2020
#---------------------------------------------------------------------------

# Define parameters which will be used for creating a delivery group for several machine catalogs
[CmdletBinding()]
param
(
    # Desktop Group properties
    [Parameter(Mandatory=$true)] [string] $deliveryGroupName,
    [Parameter(Mandatory=$true)] [string[]] $userSIDs, # Array of AD user SIDs
    [Parameter(Mandatory=$true)] [string] $catalogName # Array of machine catalogs to assign
)

# Set necessary variables
$adminAddress = "xendesktop.win2016s01.fhswf.de";

# Change to SilentlyContinue to avoid verbose output
$VerbosePreference = "Continue"

# Create the Desktop Group
If (!(Get-BrokerDesktopGroup -Name $deliveryGroupName -ErrorAction SilentlyContinue))
{
    Write-Verbose "Creating new Desktop Group: $deliveryGroupName"
    $desktopGroup = New-BrokerDesktopGroup -ErrorAction SilentlyContinue -AdminAddress $adminAddress -Name $deliveryGroupName -DeliveryType 'Desktops' -Description $deliveryGroupName -PublishedName $deliveryGroupName -MinimumFunctionalLevel 'L7_9' -SessionSupport 'SingleSession' -ShutdownDesktopsAfterUse $True -InMaintenanceMode $False -IsRemotePC $False -SecureIcaRequired $False -TurnOnAddedMachine $False -OffPeakDisconnectAction Suspend -OffPeakDisconnectTimeout 15 -Scope @()
}

# At this point, we have a Desktop Group, but no users or desktops assigned to it, no power management etc.
# Open the properties of the new Desktop Group to see what's missing.
# If creation of the desktop group was successful, continue modifying its properties
If ($desktopGroup)
{
    # Add machines to the new desktop group. Uses the number of machines available in the target machine catalog
    Write-Verbose "Getting details for the Machine Catalog: $catalogName"
    $machineCatalog = Get-BrokerCatalog -AdminAddress $adminAddress -Name $catalogName
    Write-Verbose "Adding $catalogName.UnassignedCount machines to the Desktop Group: $deliveryGroupName"
    $machinesCount = Add-BrokerMachinesToDesktopGroup -AdminAddress $adminAddress -Catalog $catalogName -Count $catalogName.UnassignedCount -DesktopGroup $desktopGroup

    # Create a new broker user/group object if it doesn't already exist
    Write-Verbose "Creating user/group object in the broker"
    foreach ($userSID in $userSIDs)
    {
        If (!(Get-BrokerUser -AdminAddress $adminAddress -SID $userSID -ErrorAction SilentlyContinue))
        {
            $brokerUsers = New-BrokerUser -AdminAddress $adminAddress -SID $userSID
        }
        Else
        {
            $brokerUsers = Get-BrokerUser -AdminAddress $adminAddress -SID $userSID
        }
    }

    # Create an entitlement policy for the new desktop group. Assigned users to the desktop group
    # First check that we have an entitlement name available. Increment until we do.
    $Num = 1
    Do
    {
        $Test = Test-BrokerEntitlementPolicyRuleNameAvailable -AdminAddress $adminAddress -Name @($deliveryGroupName + "_" + $Num.ToString()) -ErrorAction SilentlyContinue
        If ($Test.Available -eq $False) { $Num = $Num + 1 }
    }
    While ($Test.Available -eq $False)

    Write-Verbose "Assigning $brokerUsers.Name to Desktop Catalog: $catalogName"
    $EntPolicyRule = New-BrokerEntitlementPolicyRule -AdminAddress $adminAddress  -Name ($deliveryGroupName + "_" + $Num.ToString()) -IncludedUsers $brokerUsers -DesktopGroupUid $desktopGroup.Uid -Enabled $True -IncludedUserFilterEnabled $False

    # Check whether access rules exist and then create rules for direct access and via Access Gateway
    $accessPolicyRule = $deliveryGroupName + "_Direct"
    If (Test-BrokerAccessPolicyRuleNameAvailable -AdminAddress $adminAddress -Name @($accessPolicyRule) -ErrorAction SilentlyContinue)
    {
        Write-Verbose "Allowing direct access rule to the Desktop Catalog: $catalogName"
        New-BrokerAccessPolicyRule -AdminAddress $adminAddress -Name $accessPolicyRule  -IncludedUsers @($brokerUsers.Name) -AllowedConnections 'NotViaAG' -AllowedProtocols @('HDX','RDP') -AllowRestart $True -DesktopGroupUid $desktopGroup.Uid -Enabled $True -IncludedSmartAccessFilterEnabled $True -IncludedUserFilterEnabled $True
    }
    Else
    {
        Write-Error "Failed to add direct access rule $accessPolicyRule. It already exists."
    }

    $accessPolicyRule = $deliveryGroupName + "_AG"
    If (Test-BrokerAccessPolicyRuleNameAvailable -AdminAddress $adminAddress -Name @($accessPolicyRule) -ErrorAction SilentlyContinue)
    {
        Write-Verbose "Allowing access via Access Gateway rule to the Desktop Catalog: $catalogName"
        New-BrokerAccessPolicyRule -AdminAddress $adminAddress -Name $accessPolicyRule -IncludedUsers @($brokerUsers.Name) -AllowedConnections 'ViaAG' -AllowedProtocols @('HDX','RDP') -AllowRestart $True -DesktopGroupUid $desktopGroup.Uid -Enabled $True -IncludedSmartAccessFilterEnabled $True -IncludedSmartAccessTags @() -IncludedUserFilterEnabled $True
    }
    Else
    {
        Write-Error "Failed to add Access Gateway rule $accessPolicyRule. It already exists."
    }
}