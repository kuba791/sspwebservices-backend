#---------------------------------------------------------------------------
# Author:      Aaron Parker
# Desc:        Using PowerShell to create a XenDesktop 7.x machine catalog with Active Directory computer accounts
# Date:        Aug 19, 2014
# Site:        http://stealthpuppy.com
# Editor:      Kevin Kandziora
# Update-date: Nov 15, 2020
#---------------------------------------------------------------------------

[CmdletBinding()]
param
(
    [Parameter(Mandatory=$true)] [string] $catalogName,
    [Parameter(Mandatory=$true)] [string] $description,
    [Parameter(Mandatory=$true)] [string] $namingScheme,
    [Parameter(Mandatory=$true)] [string] $namingSchemeType,
    [Parameter(Mandatory=$true)] [string] $orgUnit,
    [Parameter(Mandatory=$true)] [int] $vCpuCount,
    [Parameter(Mandatory=$true)] [int] $vRAM,
    [Parameter(Mandatory=$true)] [int] $countOfAccounts
)

# Add Citrix necessary powershell snap-ins
Add-PSSnapin Citrix*

# Set necessary variables
$adminAddress = "xendesktop.win2016s01.fhswf.de";
$domain = "win2016s01.fhswf.de"
$masterImage = "Windows10_template*"
$storageResource = "vda_network"
# In this case the xdControllers are the same as the admin address
$xdControllers = $adminAddress

Write-Output "Creating machine catalog. Name: $catalogName; Description: $description; Allocation: Static"

# --- IDENTITY POOL
# Remove identitypool that may exist - REMOVE THIS LINE IF THERE IS A METHOD FOR PREVENTING THIS
Remove-AcctIdentityPool -IdentityPoolName $catalogName -ErrorAction Ignore
# Create new identity pool
$identPool = New-AcctIdentityPool -AdminAddress $adminAddress -AllowUnicode -Domain $domain -IdentityPoolName $catalogName -NamingScheme $namingScheme -NamingSchemeType $namingSchemeType -OU $orgUnit
# Check if identity pool already exists
if ($null -eq $identPool.IdentityPoolName)
{
    Write-Error "Identity pool already existing"
    Write-Error -3 -ErrorAction Stop
}

# --- BROKER CATALOG
# Create new broker catalog
New-BrokerCatalog -AdminAddress $adminAddress -AllocationType 'Static' -Description $description -Name $catalogName -PersistUserChanges 'OnLocal' -ProvisioningType 'MCS' -SessionSupport 'SingleSession' -IsRemotePC $false -MinimumFunctionalLevel 'L7_20'

# Create some metadata for the catalog
$catalogUid = (Get-BrokerCatalog | Where-Object { $_.Name -eq $catalogName } | Select-Object Uid).Uid
$guid = [guid]::NewGuid()
Set-BrokerCatalogMetadata -AdminAddress $adminAddress -CatalogId $catalogUid -Name "$catalogName - Citrix_DesktopStudio_IdentityPoolUid" -Value $guid

# --- PROVISIONING SCHEME
# Check if provisioning scheme already exists
If ($null -ne (Get-ProvScheme -ProvisioningSchemeName $catalogName))
{
    Write-Error "Provisioning Scheme already existing"
    Write-Error -4 -ErrorAction Stop
}

# Check to see whether a provisioning scheme is already available
Write-Output "Checking whether the provisioning scheme name is unused."
If (Test-ProvSchemeNameAvailable -AdminAddress $adminAddress -ProvisioningSchemeName @($catalogName))
{
    Write-Output "Success."

    # Get the master VM image from the same storage resource we're going to deploy to. Could pull this from another storage resource available to the host
    Write-Output "Getting the master image details for the new catalog: $masterImage"
    $VM = Get-ChildItem -AdminAddress $adminAddress "XDHyp:\HostingUnits\$storageResource" | Where-Object { $_.ObjectType -eq "VM" -and $_.PSChildName -like $masterImage }
    Write-Output "Found VM: $VM"
    # Get the snapshot details. This code will assume a single snapshot exists - could add additional checking to grab last snapshot or check for no snapshots.
    $VMDetails = Get-ChildItem $VM.FullPath
    Write-Output "VM details of current VM:"
    Write-Output "$VMDetails"

    # Create a new provisioning scheme - the configuration of VMs to deploy. This will copy the master image to the target datastore.
    Write-Output "Creating new provisioning scheme using $VMDetails.FullPath"
    # Provision VMs based on the selected snapshot.
    $provTaskId = New-ProvScheme -AdminAddress $adminAddress -ProvisioningSchemeName $catalogName -HostingUnitName $storageResource -MasterImageVM $VMDetails.FullPath -CleanOnBoot -IdentityPoolName $identPool.IdentityPoolName -VMCpuCount $vCpuCount -VMMemoryMB $vRAM -RunAsynchronously
    $provTask = Get-ProvTask -AdminAddress $adminAddress -TaskId $provTaskId

    # Track the progress of copying the master image - CHECK IF NEEDED (Line 53 - 60 excluding line 59 but the while-loop)
    Write-Output "Tracking progress of provisioning scheme creation task."
    $totalPercent = 0
    While ($provTask.Active -eq $True)
    {
        Try { $totalPercent = If ($provTask.TaskProgress) { $provTask.TaskProgress } Else {0} } Catch { }

        Write-Progress -Activity "Creating Provisioning Scheme (copying and composing master image):" -Status "$totalPercent% Complete:" -percentcomplete $totalPercent
        Sleep 15
        $provTask = Get-ProvTask -AdminAddress $adminAddress -TaskID $provTaskId
    }

    # If provisioning task fails, there's no point in continuing further.
    If ($provTask.WorkflowStatus -eq "Completed")
    {
        # Apply the provisioning scheme to the machine catalog
        Write-Output "Binding provisioning scheme to the new machine catalog"
        $provScheme = Get-ProvScheme | Where-Object { $_.ProvisioningSchemeName -eq $catalogName }
        Set-BrokerCatalog -AdminAddress $adminAddress -Name $provScheme.ProvisioningSchemeName -ProvisioningSchemeId $provScheme.ProvisioningSchemeUid

        # Associate a specific set of controllers to the provisioning scheme. This steps appears to be optional.
        Write-Output "Associating controllers $xdControllers to the provisioning scheme."
        Add-ProvSchemeControllerAddress -AdminAddress $adminAddress -ControllerAddress @($xdControllers) -ProvisioningSchemeName $provScheme.ProvisioningSchemeName

        # Provisiong the actual machines and map them to AD accounts, track the progress while this is happening
        Write-Output "Creating the machine accounts in AD."
        ##### AD ACCOUNT CREATION WITH PROVIDED AMOUNT #####
        $adAccounts = New-AcctADAccount -AdminAddress $adminAddress -Count $countOfAccounts -IdentityPoolUid $identPool.IdentityPoolUid
        Write-Output "Creating the virtual machines."
        $provTaskId = New-ProvVM -AdminAddress $adminAddress -ADAccountName @($adAccounts.SuccessfulAccounts) -ProvisioningSchemeName $provScheme.ProvisioningSchemeName -RunAsynchronously
        $provTask = Get-ProvTask -AdminAddress $adminAddress -TaskId $provTaskId
        # CHECK IF DISPLAYING THE PERCENTAGE IS NEEDED (Lines 88, 91, 92, 93)
        Write-Output "Tracking progress of the machine creation task."
        $totalPercent = 0
        While ($provTask.Active -eq $True)
        {
            Try { $totalPercent = If ($provTask.TaskProgress) { $provTask.TaskProgress } Else {0} } Catch { }

            Write-Progress -Activity "Creating Virtual Machines:" -Status "$totalPercent% Complete:" -percentcomplete $totalPercent
            Sleep 15
            $ProvTask = Get-ProvTask -AdminAddress $adminAddress -TaskID $provTaskId
        }

        # Assign the newly created virtual machines to the machine catalog
        $provVMs = Get-ProvVM -AdminAddress $adminAddress -ProvisioningSchemeUid $provScheme.ProvisioningSchemeUid
        Write-Output "Assigning the virtual machines to the new machine catalog."
        ForEach ($provVM in $provVMs)
        {
            Write-Output "Locking VM $provVM.ADAccountName"
            Lock-ProvVM -AdminAddress $adminAddress -ProvisioningSchemeName $provScheme.ProvisioningSchemeName -Tag 'Brokered' -VMID @($provVM.VMId)
            Write-Output "Adding VM $provVM.ADAccountName"
            New-BrokerMachine -AdminAddress $adminAddress -CatalogUid $catalogUid -MachineName $provVM.ADAccountName
        }
        Write-Output "Machine catalog creation complete."
        # Return new catalog UID for saving in DB
        Write-Output $catalogUid -InformationAction Stop
    }
    Else
    {
        # If provisioning task fails, provide error
        # Check that the hypervisor management and storage resources do no have errors. Run 'Test Connection', 'Test Resources' in Citrix Studio
        Write-Error "Provisioning task failed with error: [$provTask.TaskState] $provTask.TerminatingError"
        Write-Error -2 -ErrorAction Stop
    }
}