#---------------------------------------------------------------------------
# Author:      Aaron Parker
# Desc:        Using PowerShell to create a XenDesktop 7.x machine catalog
# Date:        Aug 19, 2014
# Site:        http://stealthpuppy.com
# Editor:      Kevin Kandziora
# Update-date: Nov 15, 2020
#---------------------------------------------------------------------------

# Define parameters which will be used for creating the machine catalog
[CmdletBinding()]
param
(
    # Administrator parameters - parameters which should be set in the specifc configuration file
    [Parameter(Mandatory=$true)] [string] $adminAddress, #The XD Controller we're going to execute against
    [Parameter(Mandatory=$true)] [string] $xdControllers,

    # Hypervisor and storage resources
    # These need to be configured in Studio prior to running this script
    # This script is hypervisor and management agnostic - just point to the right infrastructure
    [Parameter(Mandatory=$true)] [string] $storageResource, #Storage
    [Parameter(Mandatory=$true)] [string] $hostResource, #Hypervisor management

    # Machine catalog properties
    [Parameter(Mandatory=$true)] [string] $machineCatalogName,
    [Parameter(Mandatory=$true)] [string] $machineCatalogDesc,
    [Parameter(Mandatory=$true)] [string] $domain,
    [Parameter(Mandatory=$true)] [string] $orgUnit,
    [Parameter(Mandatory=$true)] [string] $namingScheme, #AD machine account naming conventions
    [Parameter(Mandatory=$true)] [string] $namingSchemeType, #'Numeric' or 'Alphabetic'
    [Parameter(Mandatory=$true)] [string] $allocType, #'Random' or 'Static' -> This parameter will always be 'Static' in our test system
    [Parameter(Mandatory=$true)] [string] $persistChanges, #'Discard', 'OnLocal' or 'OnPvD'
    [Parameter(Mandatory=$true)] [string] $provType, #'MCS', 'Manual' or 'PVS' -> This parameter will always be 'MCS' in our test system
    [Parameter(Mandatory=$true)] [string] $sessionSupport, #'SingleSession' or 'MultiSession' -> This parameter will always be 'SingleSession' in our test system
    [Parameter(Mandatory=$true)] [string] $masterImage, #Path or name of the master or golden image -> This will only be used as a like argument
    [Parameter(Mandatory=$true)] [int] $vmAmount, #Amount of vMs to create
    [Parameter(Mandatory=$true)] [int] $vCPUs, #Amount of CPUs of each VM
    [Parameter(Mandatory=$true)] [int] $vRAM #Amount of memory of each VM
)

# Change to SilentlyContinue to avoid verbose output
$VerbosePreference = "Continue"

# Load the Citrix PowerShell modules
Write-Verbose "Loading Citrix XenDesktop modules."
Add-PSSnapin Citrix*

# Get information from the hosting environment via the XD Controller
# Get the storage resource
Write-Verbose "Gathering storage and hypervisor connections from the XenDesktop infrastructure."
#$hostingUnit = Get-ChildItem -AdminAddress $adminAddress "XDHyp:\HostingUnits" | Where-Object { $_.PSChildName -like $storageResource } | Select-Object PSChildName, PsPath -> DOESN'T WORKED FOR ME
$hostingUnit = Get-ChildItem "XDHyp:\HostingUnits" | Where-Object { $_.PSChildName -like $storageResource } | Select-Object PSChildName, PsPath
# Get the hypervisor management resources
#$hostConnection = Get-ChildItem -AdminAddress $adminAddress "XDHyp:\Connections" | Where-Object { $_.PSChildName -like $hostResource } -> DOESN'T WORKED FOR ME
$hostConnection = Get-ChildItem "XDHyp:\Connections" | Where-Object { $_.PSChildName -like $hostResource }
$brokerHypConnection = Get-BrokerHypervisorConnection -AdminAddress $adminAddress -HypHypervisorConnectionUid $hostConnection.HypervisorConnectionUid
$brokerServiceGroup = Get-ConfigServiceGroup -AdminAddress $adminAddress -ServiceType 'Broker' -MaxRecordCount 2147483647

# Create a Machine Catalog. In this case a catalog with staticly assigned desktops
Write-Verbose "Creating machine catalog. Name: $machineCatalogName; Description: $machineCatalogDesc; Allocation: Static"
$brokerCatalog = New-BrokerCatalog -AdminAddress $adminAddress -AllocationType 'Static' -Description $machineCatalogDesc -Name $machineCatalogName -PersistUserChanges $persistChanges -ProvisioningType 'MCS' -SessionSupport 'SingleSession'
# The identity pool is used to store AD machine accounts
Write-Verbose "Creating a new identity pool for machine accounts."
Remove-AcctIdentityPool -IdentityPoolName $machineCatalogName
$identPool = New-AcctIdentityPool -AdminAddress $adminAddress -Domain $domain -IdentityPoolName $machineCatalogName -NamingScheme $namingScheme -NamingSchemeType $namingSchemeType -OU $orgUnit

# Creates/Updates metadata key-value pairs for the catalog (no idea why).
Write-Verbose "Retrieving the newly created machine catalog."
$catalogUid = Get-BrokerCatalog | Where-Object { $_.Name -eq $machineCatalogName } | Select-Object Uid
$guid = [guid]::NewGuid()
Write-Verbose "Updating metadata key-value pairs for the catalog."
Set-BrokerCatalogMetadata -AdminAddress $adminAddress -CatalogId $catalogUid.Uid -Name 'Citrix_DesktopStudio_IdentityPoolUid' -Value $guid

# Check to see whether a provisioning scheme is already available
Write-Verbose "Checking whether the provisioning scheme name is unused."
If (Test-ProvSchemeNameAvailable -AdminAddress $adminAddress -ProvisioningSchemeName @($machineCatalogName))
{
    Write-Verbose "Success."

    # Get the master VM image from the same storage resource we're going to deploy to. Could pull this from another storage resource available to the host
    Write-Verbose "Getting the master image details for the new catalog: $masterImage"
    $VM = Get-ChildItem -AdminAddress $adminAddress "XDHyp:\HostingUnits\$storageResource" | Where-Object { $_.ObjectType -eq "VM" -and $_.PSChildName -like $masterImage }
    # Get the snapshot details. This code will assume a single snapshot exists - could add additional checking to grab last snapshot or check for no snapshots.
    $VMDetails = Get-ChildItem -AdminAddress $adminAddress $VM.FullPath

    # Create a new provisioning scheme - the configuration of VMs to deploy. This will copy the master image to the target datastore.
    Write-Verbose "Creating new provisioning scheme using $VMDetails.FullPath"
    # Provision VMs based on the selected snapshot.
    $provTaskId = New-ProvScheme -AdminAddress $adminAddress -ProvisioningSchemeName $machineCatalogName -HostingUnitName $storageResource -MasterImageVM $VMDetails.FullPath -CleanOnBoot -IdentityPoolName $identPool.IdentityPoolName -VMCpuCount $vCPUs -VMMemoryMB $vRAM -RunAsynchronously
    $provTask = Get-ProvTask -AdminAddress $adminAddress -TaskId $provTaskId

    # Track the progress of copying the master image
    Write-Verbose "Tracking progress of provisioning scheme creation task."
    $totalPercent = 0
    While ( $provTask.Active -eq $True ) {
        Try { $totalPercent = If ( $provTask.TaskProgress ) { $provTask.TaskProgress } Else {0} } Catch { }

        Write-Progress -Activity "Creating Provisioning Scheme (copying and composing master image):" -Status "$totalPercent% Complete:" -percentcomplete $totalPercent
        Sleep 15
        $provTask = Get-ProvTask -AdminAddress $adminAddress -TaskID $provTaskId
    }

    # If provisioning task fails, there's no point in continuing further.
    If ( $provTask.WorkflowStatus -eq "Completed" )
    {
        # Apply the provisioning scheme to the machine catalog
        Write-Verbose "Binding provisioning scheme to the new machine catalog"
        $provScheme = Get-ProvScheme | Where-Object { $_.ProvisioningSchemeName -eq $machineCatalogName }
        Set-BrokerCatalog -AdminAddress $adminAddress -Name $provScheme.ProvisioningSchemeName -ProvisioningSchemeId $provScheme.ProvisioningSchemeUid

        # Associate a specific set of controllers to the provisioning scheme. This steps appears to be optional.
        Write-Verbose "Associating controllers $xdControllers to the provisioning scheme."
        Add-ProvSchemeControllerAddress -AdminAddress $adminAddress -ControllerAddress @($xdControllers) -ProvisioningSchemeName $provScheme.ProvisioningSchemeName

        # Provisiong the actual machines and map them to AD accounts, track the progress while this is happening
        Write-Verbose "Creating the machine accounts in AD."
        $adAccounts = New-AcctADAccount -AdminAddress $adminAddress -Count $vmAmount -IdentityPoolUid $identPool.IdentityPoolUid
        Write-Verbose "Creating the virtual machines."
        $provTaskId = New-ProvVM -AdminAddress $adminAddress -ADAccountName @($adAccounts.SuccessfulAccounts) -ProvisioningSchemeName $provScheme.ProvisioningSchemeName -RunAsynchronously
        $provTask = Get-ProvTask -AdminAddress $adminAddress -TaskId $provTaskId

        Write-Verbose "Tracking progress of the machine creation task."
        $totalPercent = 0
        While ( $provTask.Active -eq $True ) {
            Try { $totalPercent = If ( $provTask.TaskProgress ) { $provTask.TaskProgress } Else {0} } Catch { }

            Write-Progress -Activity "Creating Virtual Machines:" -Status "$totalPercent% Complete:" -percentcomplete $totalPercent
            Sleep 15
            $ProvTask = Get-ProvTask -AdminAddress $adminAddress -TaskID $provTaskId
        }

        # Assign the newly created virtual machines to the machine catalog
        $provVMs = Get-ProvVM -AdminAddress $adminAddress -ProvisioningSchemeUid $provScheme.ProvisioningSchemeUid
        Write-Verbose "Assigning the virtual machines to the new machine catalog."
        ForEach ( $provVM in $provVMs ) {
            Write-Verbose "Locking VM $provVM.ADAccountName"
            Lock-ProvVM -AdminAddress $adminAddress -ProvisioningSchemeName $provScheme.ProvisioningSchemeName -Tag 'Brokered' -VMID @($provVM.VMId)
            Write-Verbose "Adding VM $provVM.ADAccountName"
            New-BrokerMachine -AdminAddress $adminAddress -CatalogUid $catalogUid.Uid -MachineName $provVM.ADAccountName
        }
        Write-Verbose "Machine catalog creation complete."

    } Else {
        # If provisioning task fails, provide error
        # Check that the hypervisor management and storage resources do no have errors. Run 'Test Connection', 'Test Resources' in Citrix Studio
        Write-Error "Provisioning task failed with error: [$provTask.TaskState] $provTask.TerminatingError"
    }
}